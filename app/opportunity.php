<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class opportunity extends Model
{
    protected $guarded = ['id'];
    public function organization()
    {
        return $this->belongsTo('App\organization');
    }
}
