<?php
use App\setting;

/**
 * This file contains additional functions.
 */

/**
 * webcrm_token
 * This function returns the latest webCRM token stored in the Database.
 */
if(!function_exists('webcrm_token')) {
    function webcrm_token() {
        $setting = setting::latest()->first();
        if(is_null($setting)) {
            return back()->with('info','There seems to be no API token for webCRM. Please refresh the CRM connection.');
        }
        $token = $setting->apikey;

        if(is_null($token)) {
            return back()->with('Could not find a token. Please refresh the CRM Connection.');
        }
        return $token;
    }
}


?>