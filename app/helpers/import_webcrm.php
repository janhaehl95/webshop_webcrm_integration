<?php

use App\linkeditem;
use App\organization;
use App\opportunity;
use App\delivery;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Carbon\carbon;
/**
 * This file handles all functions to import 
 * single entities from webCRM.
 */


/**import_linkeddata
 * 
 * Imports all linkedDataItems from webCRM
 * @params none
 * @return error array
 */
if(!function_exists('import_linkeddata')) {
    function import_linkeddata() {
        //Create output array
        $errors = array();

        //Create Client
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
        ];

        //Request
        try {
            //One request is limited to 50 -> to be on the safe side, we will iterate through pages
                $request = $client->request('GET','https://api.webcrm.com/QuotationLines/LinkedData',[
                    'headers' => $headers
                ]);
                //Get Response
                $response = json_decode($request->getBody(),true);

                //Now, the response is not empty, so we need to import.
                foreach ($response as $r) {
                    //Create new linkedItem or update existing
                    $linkeditem = linkeditem::updateOrCreate(['webcrm_id' => $r['QuotationLineLinkedDataItemId']],
                    [
                        'item_group' => $r['QuotationLineLinkedDataItemItemGroup'],
                        'economic_item_number' => $r['QuotationLineLinkedDataItemData1'],
                        'economic_item_description' => $r['QuotationLineLinkedDataItemData2'],
                        'description' => $r['QuotationLineLinkedDataItemDataMemo'],
                        'item_price' => $r['QuotationLineLinkedDataItemPrice']
                    ]
                    );
                }
            }
        
        catch (RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            return $errors;
        }


    }
}

/**import opportunities
 * Import/Update all opportunities
 * @params 
 * @return error array (empty if no errors)
 */
if(!function_exists('import_opportunities')) {
    function import_opportunities() {
        //Output array
        $errors = array();

        //Init Client
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
        ];


        $loop_active = true;
        $x = 1;
        try {
            while ($loop_active) {
                    $request = $client->request('GET', 'https://api.webcrm.com//Opportunities?page='.$x, [
                    'headers' => $headers
                ]);
                //Get Response
                $response = json_decode($request->getBody(), true);

                if(count($response)<1) {
                    break;
                }

                //Link to organization
                
            
                //Fill Data
                foreach ($response as $r) {
                    $organization = organization::where('webcrm_id','like',$r['OpportunityOrganisationId'])->first();
                    if(is_null($organization)) {
                        continue;
                    }
                    $opportunity = opportunity::updateOrCreate(
                        ['webcrm_id' => $r['OpportunityId']],
                    [
                    'organization_id' => $organization->id,
                    'level' => $r['OpportunityLevelText'],
                    'created_by' => $r['OpportunityCreatedBy'],
                    'test_before_order' => $r['OpportunityCustom1'],
                    'description' => $r['OpportunityDescription'],
                    'webcrm_created_at' => Carbon::parse($r['OpportunityCreatedAt'])
                    ]
                );
                }

                $x++;
            } #End While
            
        }
        catch (RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            return $errors;
        }

        //All Done, return
        return $errors;
    }
}

/**import deliveries
 * Import/Update all deliveries
 * @params 
 * @return error array (empty if no errors)
 */
if(!function_exists('import_deliveries')) {
    function import_deliveries() {
        //Output array
        $errors = array();

        //Init Client
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
        ];


        $loop_active = true;
        $x = 1;
        try {
            while ($loop_active) {
                    $request = $client->request('GET', 'https://api.webcrm.com//Deliveries?page='.$x, [
                    'headers' => $headers
                ]);
                //Get Response
                $response = json_decode($request->getBody(), true);

                if(count($response)<1) {
                    break;
                }

                //Link to organization
                
            
                //Fill Data
                foreach ($response as $r) {
                    $organization = organization::where('webcrm_id','like',$r['DeliveryOrganisationId'])->first();
                    if(is_null($organization)) {
                        continue;
                    }
                    $delivery = delivery::updateOrCreate(
                        ['webcrm_id' => $r['DeliveryId']],
                    [
                    'organization_id' => $organization->id,
                    'level' => $r['DeliveryStatus'], #???
                    'created_by' => $r['DeliveryCreatedBy'],
                    'test_before_order' => $r['DeliveryCustom1'],
                    'description' => $r['DeliveryDescription'],
                    'webcrm_created_at' => Carbon::parse($r['DeliveryCreatedAt'])
                    ]
                );
                }

                $x++;
            } #End While
            
        }
        catch (RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            return $errors;
        }

        //All Done, return
        return $errors;
    }
}

/**import_organizations
 * Import all organizations from webCRM
 */
if(!function_exists('import_organizations')) {
    function import_organizations() {
        //Output array
        $errors = array();

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
        ];
        
        try{
            /**
             * We need to iterate through pages, per page -> 50
             * So we need to iterate until the response is empty (empty body)
             * $x is the iterator
             */
            $x = 30;
            $loop_active = true;
            while($x>0) {
                $request = $client->request('GET','https://api.webcrm.com/Organisations?page='.$x,[
                    'headers' => $headers
                ]);
                //Get Response
                $response = json_decode($request->getBody(),true);


                //The response is not empty, so we will import / update the organization
                if (!empty($response)) {
                    foreach ($response as $r) {
                        //Is there already an organization?
                    
                        $organization = organization::updateOrCreate(['webcrm_id' => $r['OrganisationId']]);

                        $organization->city = $r['OrganisationCity'];
                        $organization->country = $r['OrganisationCountry'];
                        $organization->address = $r['OrganisationAddress'];
                        $organization->postcode = $r['OrganisationPostCode'];
                        $organization->created_at = Carbon::parse($r['OrganisationCreatedAt']);
                        $organization->updated_at = Carbon::now();
                        $organization->created_by = $r['OrganisationCreatedBy'];
                        $organization->division_name = $r['OrganisationDivisionName'];
                        $organization->customer_number = $r['OrganisationExtraCustom2'];
                        $organization->webcrm_id = $r['OrganisationId'];
                        $organization->name = $r['OrganisationName'];
                        $organization->status = $r['OrganisationStatus'];
                        $organization->phone = $r['OrganisationTelephone'];
                        $organization->url = $r['OrganisationWww'];
                        $organization->email_invoice = $r['OrganisationExtraCustom3'];
                        $organization->email_cardiomatics = $r['OrganisationCustom2'];
                        $organization->email_contact = $r['OrganisationCustom6'];
                        $organization->save();
                    }
                }

                //Update iterator for next
                $x--;
            }
        }
        catch (RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            return $errors;
        }

        return $errors; //Should be empty..
    }
}

/**import_single_organization
 * Import a single organization from webCRM by its webcrm id
 */
if(!function_exists('import_single_organization')) {
    function import_single_organization($webcrm_id) {
        //Output array
        $errors = array();

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
        ];
        
        try{
            /**
             * We need to iterate through pages, per page -> 50
             * So we need to iterate until the response is empty (empty body)
             * $x is the iterator
             */
                $request = $client->request('GET','https://api.webcrm.com/Organisations/'.$webcrm_id,[
                    'headers' => $headers
                ]);
                //Get Response
                $response = json_decode($request->getBody(),true);


                //The response is not empty, so we will import / update the organization
                if (!empty($response)) {
                    $r = $response;
                        //Is there already an organization?
                    
                        $organization = organization::updateOrCreate(['webcrm_id' => $r['OrganisationId']]);

                        $organization->city = $r['OrganisationCity'];
                        $organization->country = $r['OrganisationCountry'];
                        $organization->address = $r['OrganisationAddress'];
                        $organization->postcode = $r['OrganisationPostCode'];
                        $organization->created_at = Carbon::parse($r['OrganisationCreatedAt']);
                        $organization->updated_at = Carbon::now();
                        $organization->created_by = $r['OrganisationCreatedBy'];
                        $organization->division_name = $r['OrganisationDivisionName'];
                        $organization->customer_number = $r['OrganisationExtraCustom2'];
                        $organization->webcrm_id = $r['OrganisationId'];
                        $organization->name = $r['OrganisationName'];
                        $organization->status = $r['OrganisationStatus'];
                        $organization->phone = $r['OrganisationTelephone'];
                        $organization->url = $r['OrganisationWww'];
                        $organization->email_invoice = $r['OrganisationExtraCustom3'];
                        $organization->email_cardiomatics = $r['OrganisationCustom2'];
                        $organization->email_contact = $r['OrganisationCustom6'];
                        $organization->save();
                    
                }
            
        }
        catch (RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            return $errors;
        }

        return $errors; //Should be empty..
    }
}

/**import single opportunity
 * Import/Update all opportunities
 * @params 
 * @return error array (empty if no errors)
 */
if(!function_exists('import_single_opportunity')) {
    function import_single_opportunity($webcrm_id,$organization_id) {
        //Output array
        $errors = array();

        //Init Client
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
        ];
        try {
                $request = $client->request('GET', 'https://api.webcrm.com/Opportunities/'.$webcrm_id, [
                    'headers' => $headers
                ]);
                //Get Response
                $response = json_decode($request->getBody(), true);

                //Fill Data
                $r = $response;
                    
                    $opportunity = opportunity::updateOrCreate(
                        ['webcrm_id' => $r['OpportunityId']],
                    [
                    'organization_id' => $organization_id,
                    'level' => $r['OpportunityLevelText'],
                    'created_by' => $r['OpportunityCreatedBy'],
                    'test_before_order' => $r['OpportunityCustom1'],
                    'description' => $r['OpportunityDescription'],
                    'webcrm_created_at' => Carbon::parse($r['OpportunityCreatedAt'])
                    ]
                );
        }
        catch (RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            return $errors;
        }

        //All Done, return
        return $errors;
    }
}
?>