<?php

/**
 * This file contains all individiaul functions to sync an order to webCRM.
 * It is mainly called by the webcrmcontorller.
 */
use App\linkeditem;
use App\mca;
use App\organization;
use App\opportunity;
use App\delivery;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Carbon\carbon;


if(!function_exists('create_organization')) {
    function create_organization($order) {
        $errors = array();

        /**
         * Set some values for the order
         */
        $lead_from = "00:--Choose--";
        $customer_through = "[0]"; #None
        $mca_code = "-";
        if($order->mca_code) {
            //Is a Pfizer Customer
            $lead_from = "08: Pfizer";
            $mca_code = $order->mca_code;
            $customer_through = "[1]"; #Pfizer
        }
        #Cardiomatics E-Mail?
            #Is there a cardiomatics product in the order and the Cardiomatics e-mail is not set??
            if(is_null($order->cardiomatics_email) ) {
                $order->cardiomatics_email = $order->email;
                $order->save();
            }

        //MCA
        #01: Joerg.Bruechmann@pfizer.com

        $mca_email_string = '00:--Choose--';
        $mca_couponcode_string = "01:2001\t";
        $mca_name_string = "01: Jörg Brüchmann\t";

        $mca = mca::find($order->mca_id);
        if(!is_null($mca)) {
            $mca_email_string = $mca->webcrm_number.": ".$mca->email;
            $mca_couponcode_string = $mca->webcrm_number.": ".$mca->coupon_code."\t";
            $mca_name_srtring = $mca->webcrm_number.": ".$mca->name."\t";
        }

        //Refresh order
        $order->fresh();

        #$token = env('api_token');
        $client = new Client(['base_uri' => 'https://api.webcrm.com/Organisations']);
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
            'Content-Type'        => 'application/json',
        ];
        $client = new Client();
        
        try {
            $response = $client->request('POST','https://api.webcrm.com/Organisations',[
                'headers' => $headers,
                'body' => json_encode([
                    'OrganisationAddress' => $order->billing_address_1." ".$order->billing_address_2,
                    'OrganisationAlert' => 'Webshop Import',
                    'OrganisationApprovalStatus' => 0,
                    'OrganisationCity' => $order->billing_city,
                    'OrganisationCountry' => 'DE',
                    'OrganisationDivisionName' => $order->billing_first_name." ".$order->billing_last_name,
                    'OrganisationExtraCustom2' => $order->customer_id,
                    'OrganisationType' => 'Customer',
                    'OrganisationIndustry' => 'Health Care',
                    'OrganisationExtraCustom6' => 'EUR',
                    'OrganisationCustom8' => $lead_from,
                    'OrganisationId' => 0,
                    'OrganisationName' => $order->billing_company,
                    'OrganisationOwner' => 6,
                    'OrganisationOwner2' => 7,
                    'OrganisationPostCode' => $order->billing_postcode,
                    'OrganisationTelephone' => $order->phone,
                    'OrganisationExtraCustom5' => 'Netto 14 Tage', #Payment requirements
                    'OrganisationExtraCustom3' => $order->email, #Invoicing e-mail
                    'OrganisationCustom2' => $order->cardiomatics_email, #Cardiomatics e-mail
                    'OrganisationCustom1' => $mca_email_string, #MCA 01: Joerg.Bruechmann@pfizer.com
                    'OrganisationCustom10' => $mca_couponcode_string, #MCA Coupon Code "01:2001\t",
                    'OrganisationCustom11' => $mca_name_string, #MCA Name 01: Jörg Brüchmann\t
                    'OrganisationCustom6' => $order->email, #Contact e-mail
                    'OrganisationCustom9' => $customer_through #Customer Through

                ])
            ]);
            $status = $response->getStatusCode();
        }
        catch(RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),10000)[0];
            $errors[] = "Aborted because the new organization could not be created in webCRM. Try to refresh the webCRM Token and try again. Otherwise please froward this message to Jan.";
            return $errors;
        }
        #Get the webCRM Id
        $webcrm_org_id = (int) json_decode($response->getBody());

        //Import & associate this organization
        $new_organization_import = import_single_organization($webcrm_org_id);

        if(!empty($new_organization_import)) {
            
            foreach($new_organization_import as $organizaiton_import_error) {
                $errors[] = $organizaiton_import_error."...";
            }
            $errors[] = "Aborted because the new organization in webCRM failed to be called. Please forward this message to Jan.";
            return $errors;
        }
        $new_organization = organization::where('webcrm_id',$webcrm_org_id)->first();
        $order->organization_id = $new_organization->id;
        $order->save();
        return $errors;
    }
}

/**
 * create_opportunity
 */

if(!function_exists('create_opportunity')) {
    function create_opportunity($order,$organization) {
        $errors = array();

        /**
         * Set some values for the import
         */
        $mca_code = "-";
        if($order->mca_code) {
            //Is a Pfizer Customer
            $mca_code = $order->mca_code;
        }

        //Default
        $pipeline_level = 1;
        $pipeline_level_text = "[1] Prospect (0%)";
        $test_before_purchase = "[1]"; #1 = No, 2 = Yes

        //TEST Order?
        if($order->is_test_order) {
            #Test
            $pipeline_level = 3;
            $pipeline_level_text = "[3] Qualified / Test Order (20%)";
            $test_before_purchase = "[2]"; #2 = Yes
        }
        else {
            #Full Purchase
            $pipeline_level = 12;
            $pipeline_level_text = "[12] TB Invoiced (100%)";
            #We don't touch test before purchase here, as this is only set for a test order that is transferred to a full purchase. Here, let's leave it at "No" because it's a full purchase and the opportunity is created new.
        }

        //Discount
        $discount_text = "Without discount";
        if($order->discount_total > 0) {
            $discount_text = "With discount";
        }

        //MCA

        $mca_email_string = '--Choose--';
        $mca_couponcode_string = "--Choose--";
        $mca_name_string = "--Choose--";

        $mca = mca::find($order->mca_id);
        if(!is_null($mca)) {
            $mca_email_string = $mca->email;
            $mca_couponcode_string = $mca->coupon_code;
            $mca_name_srtring = $mca->name;
        }

       
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
            'Content-Type'        => 'application/json',
        ];
        $client = new Client();
    

        try {
            $response = $client->request('POST','https://api.webcrm.com/Opportunities',[
                'headers' => $headers,
                'body' => json_encode([
                    'OpportunityAssignedTo' => 7,
                    'OpportunityComment' =>  'MCA Code: '.$mca_code,
                    'OpportunityCreatedBy' => 'Webshop',
                    'OpportunityOrderDate' => $order->ordered_at,
                    'OpportunityCurrencyName' => '03',
                    'OpportunityCurrencySymbol' => 'EUR',
                    'OpportunityDiscount' => 0,
                    'OpportunityId' => 0,
                    'OpportunityLevel' => $pipeline_level,
                    'OpportunityLevelText' => $pipeline_level_text,
                    'OpportunityCustom1' => $test_before_purchase, //Test before purchase 1 No 2 Yes, see above
                    'OpportunityCustom7' => $discount_text,
                    'OpportunityOrganisationId' => $organization->webcrm_id,
                    'OpportunityCustom3' => $mca_couponcode_string, #MCA CODE
                    'OpportunityCustom4' => $mca_email_string, #MCA E-Mail
                    'OpportunityCustom5' => $mca_name_string, #MCA Name

                ])
            ]);
            $status = $response->getStatusCode();
        }
        catch(RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            $errors[] = "Aborted because the new opportunity could not be created in webCRM. Try to refresh the webCRM Token and try again. Otherwise please froward this message to Jan.";
            return $errors;
        }

        $webcrm_opp_id = (int) json_decode($response->getBody());

        //Import & associate this opportunity
        $new_opportunity_import = import_single_opportunity($webcrm_opp_id,$organization->id);

        if(!empty($new_opportunity_import)) {
            
            foreach($new_opportunity_import as $opportunity_import_error) {
                $errors[] = $opportunity_import_error."...";
            }
            $errors[] = "Aborted because the new opportunity in webCRM failed to be called. Please forward this message to Jan.";
            return $errors;
        }

        $new_opportunity = opportunity::where('webcrm_id',$webcrm_opp_id)->first();
        $order->opportunity_id = $new_opportunity->id;
        $order->save();
        return $errors;
    }
}

/**
 * create_opportunity
 */

if(!function_exists('update_opportunity')) {
    function update_opportunity($opportunity,$order,$organization) {
        $errors = array();

        /**
         * Set some values for the import
         */
        $mca_code = "-";
        if($order->mca_code) {
            //Is a Pfizer Customer
            $mca_code = $order->mca_code;
        }

        //Default
        $pipeline_level = 1;
        $pipeline_level_text = "[1] Prospect (0%)";
        $test_before_purchase = "[1]"; #1 = No, 2 = Yes

        //TEST Order?
        if($order->is_test_order) {
            #Test
            $pipeline_level = 3;
            $pipeline_level_text = "[3] Qualified / Test Order (20%)";
            $test_before_purchase = "[2]"; #2 = Yes
        }
        else {
            #Full Purchase
            $pipeline_level = 11;
            $pipeline_level_text = "[12] TB Invoiced (100%)";
            /**
             * If the opportunity is on a test level now, we automatically set test before purchase to YES.
             */
            if($opportunity->level == "[3] Qualified / Test Order (20%)") {
                $test_before_purchase = "[2]"; #2 = Yes
            }
        }

        //MCA
        $mca_email_string = '--Choose--';
        $mca_couponcode_string = "--Choose--";
        $mca_name_string = "--Choose--";

        $mca = mca::find($order->mca_id);
        if(!is_null($mca)) {
            $mca_email_string = $mca->email;
            $mca_couponcode_string = $mca->coupon_code;
            $mca_name_srtring = $mca->name;
        }

        //Discount
        $discount_text = "Without discount";
        if($order->discount_total > 0) {
            $discount_text = "With discount";
        }
       
        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
            'Content-Type'        => 'application/json',
        ];
        $client = new Client();
    

        try {
            $response = $client->request('PUT','https://api.webcrm.com/Opportunities/'.$opportunity->webcrm_id,[
                'headers' => $headers,
                'body' => json_encode([
                    'OpportunityComment' =>  'MCA Code: '.$mca_code,
                    'OpportunityCreatedBy' => 'Webshop',
                    'OpportunityOrderDate' => $order->ordered_at,
                    'OpportunityCurrencyName' => '03',
                    'OpportunityCurrencySymbol' => 'EUR',
                    'OpportunityDiscount' => 0,
                    'OpportunityLevel' => $pipeline_level,
                    'OpportunityId' => $opportunity->webcrm_id,
                    'OpportunityLevelText' => $pipeline_level_text,
                    'OpportunityCustom1' => $test_before_purchase, //Test before purchase 1 No 2 Yes, see above
                    'OpportunityCustom7' => $discount_text,
                    'OpportunityCustom3' => $mca_couponcode_string, #MCA CODE
                    'OpportunityCustom4' => $mca_email_string, #MCA E-Mail
                    'OpportunityCustom5' => $mca_name_string, #MCA Name
                ])
            ]);
            $status = $response->getStatusCode();
        }
        catch(RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),250)[0];
            $errors[] = "Aborted because the new opportunity could not be created in webCRM. Try to refresh the webCRM Token and try again. Otherwise please froward this message to Jan.";
            return $errors;
        }

        //Update this opportunity (get data from webCRM)
        $new_opportunity_import = import_single_opportunity($opportunity->webcrm_id,$organization->id);

        if(!empty($new_opportunity_import)) {
            
            foreach($new_opportunity_import as $opportunity_import_error) {
                $errors[] = $opportunity_import_error."...";
            }
            $errors[] = "Aborted because updating opportunity from webCRM failed. This is however just a minor issue, the opportunity might be displayed wrong here but should be correct in webCRM. Please forward this message to Jan.";
            return $errors;
        }

        return $errors;
    }
}

if(!function_exists('create_quotation_lines')) {
    function create_quotation_lines($opportunity,$order,$organization) {
        $errors = array();

        //Get LinkedData Items
        $linkeditems = $order->linkeditems;

        $headers = [
            'Authorization' => 'Bearer ' . webcrm_token(),        
            'Content-Type'        => 'application/json',
        ];
        $client = new Client();
        
        foreach($linkeditems as $linkeditem) {
            try {
                $response = $client->request('POST','https://api.webcrm.com/QuotationLines/',[
                    'headers' => $headers,
                    'body' => json_encode([
                        "QuotationLineLinkedDataId" => $linkeditem->webcrm_id,
                        "QuotationLineComment" => "API Import",
                        "QuotationLineCreatedAt" => $order->ordered_at,
                        "QuotationLineOpportunityId" => $opportunity->webcrm_id,
                        "QuotationLineOrganisationId" => $organization->webcrm_id,
                        "QuotationLineMemo" => $linkeditem->description,
                        "QuotationLinePrice" => $linkeditem->item_price,
                        "QuotationLineId" => 0,
                        "QuotationLineDiscount" => 0.0,
                        'QuotationLineVatPercentage' => 25.0,
                    ])
                ]);
                $status = $response->getStatusCode();
            }
            catch(RequestException $e) {
                $code = $e->getCode();
                $errors[] = $code." | ".str_split($e->getMessage(),500)[0];
                $errors[] = "Aborted because a quotation line could not be created in webCRM. Please froward this message to Jan.";
                return $errors;
            }
        }

        return $errors;
    }
}

?>