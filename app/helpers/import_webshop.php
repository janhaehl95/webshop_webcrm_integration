<?php

/**
 * This document contains helper functions 
 * to import and/or update objects from the webshop.
 */

use App\order;
use App\product;
use App\variation;
use App\organization;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use PhpParser\Node\Expr\BinaryOp\Concat;

/**
 * import_products
 * Will import/update products from the webshop
 * @param none
 * @return array of error, empty if no error occured
 * throws an error when a request is not successful with the error message
 */
if(!function_exists('import_products')) {
    function import_products() {
        //Errors
        $errors = array();

        $client = new Client();
        try {
            $req = $client->get("https://cortrium.com/wp-json/wc/v3/products?per_page=100&lang=all", ['auth' => ['ck_79c050882fc32202cab85d2026b9a9ac25e66db8', 'cs_21c9b8f5bf6af415f34d35a42b7dee494f268258']]);
            $status = $req->getStatusCode();

            $response = json_decode($req->getBody(),true);

            //Loop through response and add products
            foreach($response as $r) {
                $product = product::firstOrCreate(['woocommerce_id' => $r['id']],
                [
                    'name' => $r['name'],
                    'lang' => $r['lang']
                ]
                );
                

                //Import variants
                $product->fresh(); #Refresh from db
                import_variations($product);
            }
        }
        catch(RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            return $errors;
        }
        return $errors;
    }
    
}

/**
 * import_variations
 * Will import/update variations from the webshop
 * @param product
 * @return array of errors
 */
if(!function_exists('import_variations')) {
    function import_variations(product $product) {
        //Errors
        $errors = array();

        $product_id = $product->id;
        $client = new client();
        try {
            $request = $client->get("https://cortrium.com/wp-json/wc/v3/products/".$product->woocommerce_id."/variations", ['auth' => ['ck_79c050882fc32202cab85d2026b9a9ac25e66db8', 'cs_21c9b8f5bf6af415f34d35a42b7dee494f268258']]);
            $status = $request->getStatusCode();
            if($status != 200) {
                return false;
            }
            $response = json_decode($request->getBody(),true);
            foreach($response as $r) {
                $variation = variation::firstOrCreate(['woocommerce_id' => $r['id']],
                [
                    'name' => $r['attributes'][0]['name'],
                    'option' => $r['attributes'][0]['option'],
                    'description' => $r['description'],
                    'product_id' => $product_id,
                ]
                );
                
            }
        }
        catch (RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
            return $errors;
        }
        
        return $errors;
    }
}

/** import_orders
 * Will import/update orders from the webshop
 * 
 * @param none
 * @return array of error, empty if no error occured
 * throws an error when a request is not successful with the error message
 */
if(!function_exists('import_orders')) {
    function import_orders() {
        //Errors for output
        $errors = array();

        //Create New Client
        $client = new Client();

        //Get All Orders, loop through them and add them
        $loop_active = true;

        //Determine Total Pages
        try {
            $for_total_pages = $client->get("https://cortrium.com/wp-json/wc/v3/orders?per_page=100&lang=all", ['auth' =>  ['ck_79c050882fc32202cab85d2026b9a9ac25e66db8', 'cs_21c9b8f5bf6af415f34d35a42b7dee494f268258']]);
        }
        catch (RequestException $e) {
            $code = $e->getCode();
            $errors[] = $code." | ".str_split($e->getMessage(),100)[0];
            return $errors;
        } 

        for ($x = 1; $loop_active; $x++) {
            try {
            $req = $client->get("https://cortrium.com/wp-json/wc/v3/orders/?per_page=100&page=" . $x . "&orderby=id&order=asc&lang=all", ['auth' =>  ['ck_79c050882fc32202cab85d2026b9a9ac25e66db8', 'cs_21c9b8f5bf6af415f34d35a42b7dee494f268258']]);

            /** Get Total Number and Pages
             * The total number of pages is important to know when to break.
             **/
            $total_pages = (int) $req->getHeader('X-WP-TotalPages')[0];
            if ($x > $total_pages) {
                $loop_active = false;
                break;
            }

            //Decode to get it as array
            $response = json_decode($req->getBody(), true);
            }
            catch (ErrorException $e) {
                $code = $e->getCode();
                $errors[] = $code." | ".str_split($e->getMessage(),150)[0];
                return $errors;
            }
            //Now, let's access that response

            foreach ($response as $r) {
                //Is it a subscription renewal or new order?
                $type_of_order = $r['created_via'];
                if ($type_of_order != 'checkout') {
                    continue;
                }
                //Failed / trashed / refunded order? -> Clean status
                if ($r['status'] == 'failed' || $r['status'] == 'trash' || $r['status'] == 'refunded') {
                    continue;
                }

                //Identify indexes of meta fields
                $meta_arrays = $r['meta_data'];
                $mca_code_index = 99;
                $salutation_index = 99;
                $title_index = 99;
                $cardiomatics_email_index = 99;
                $correspondance_email_index = 99;
                $language_index = 99;

                for ($y = 0; $y < count($meta_arrays); $y++) {
                    //MCA Field
                    if ($meta_arrays[$y]['key'] == "_order_mca_code") {
                        $mca_code_index = $y;
                    }
                    //Salutation
                    if ($meta_arrays[$y]['key'] == "_billing_salutation") {
                        $salutation_index = $y;
                    }
                    //Title
                    if ($meta_arrays[$y]['key'] == "_billing_title") {
                        $title_index = $y;
                    }
                    //Cardiomatics E-Mail
                    if ($meta_arrays[$y]['key'] == "_billing_e_mail_adress_cardio") {
                        $cardiomatics_email_index = $y;
                    }
                    //Correspondance E-Mail
                    if ($meta_arrays[$y]['key'] == "_billing_e_mail_correspondance") {
                        $correspondance_email_index = $y;
                    }
                    //Language
                    if ($meta_arrays[$y]['key'] == "wpml_language") {
                        $language_index = $y;
                    }
                }

                //New Order or Update
                $order = order::firstOrNew(['order_id' => $r['id']]);
                //Fill Order
                $order->order_id = $r['id'];
                $order->parent_id = $r['parent_id'];
                $order->order_key = $r['order_key'];
                $order->status = $r['status'];
                $order->currency = $r['currency'];
                $order->ordered_at = $r['date_created'];
                $order->discount_total = $r['discount_total'];
                $order->total = $r['total'];
                $order->taxes = $r['total_tax'];
                $order->customer_id = $r['customer_id'];
                $order->customer_ip = $r['customer_ip_address'];
                $order->customer_user_agent = $r['customer_user_agent'];
                $order->customer_note = $r['customer_note'];
                $order->billing_first_name = $r['billing']['first_name'];
                $order->billing_last_name = $r['billing']['last_name'];
                $order->billing_company = $r['billing']['company'];
                $order->billing_address_1 = $r['billing']['address_1'];
                $order->billing_address_2 = $r['billing']['address_2'];
                $order->billing_city = $r['billing']['city'];
                $order->billing_state = $r['billing']['state'];
                $order->billing_postcode = $r['billing']['postcode'];
                $order->billing_country = $r['billing']['country'];
                $order->email = $r['billing']['email'];
                $order->phone = $r['billing']['phone'];
                $order->shipping_first_name = $r['shipping']['first_name'];
                $order->shipping_last_name = $r['shipping']['last_name'];
                $order->shipping_company = $r['shipping']['company'];
                $order->shipping_address_1 = $r['shipping']['address_1'];
                $order->shipping_address_2 = $r['shipping']['address_2'];
                $order->shipping_city = $r['shipping']['city'];
                $order->shipping_state = $r['shipping']['state'];
                $order->shipping_postcode = $r['shipping']['postcode'];
                $order->shipping_country = $r['shipping']['country'];
                $order->payment_method = $r['payment_method'];
                $order->payment_method_title = $r['payment_method_title'];
                $order->transaction = $r['transaction_id'];
                $order->line_items = json_encode($r['line_items']);
                $order->is_test_order = false; //Will be set to true in a later step, see below
                
                //Check if these fields are found (otherwise don't put in anything, Failsafe)
                if ($salutation_index < 99) {
                    $order->salutation = $r['meta_data'][$salutation_index]['value'];
                }
                if ($title_index < 99) {
                    $order->title = $r['meta_data'][$title_index]['value'];
                }
                if ($cardiomatics_email_index < 99) {
                    $order->cardiomatics_email = $r['meta_data'][$cardiomatics_email_index]['value'];
                }
                if ($correspondance_email_index < 99) {
                    $order->contact_email = $r['meta_data'][$correspondance_email_index]['value'];
                }
                if ($mca_code_index < 99) {
                    $order->mca_code = $r['meta_data'][$mca_code_index]['value'];
                }
                if ($language_index < 99) {
                    $order->language = $r['meta_data'][$language_index]['value'];
                }
                $order->save();

                //Determine if this is a test order & Sync Products
                $wc_products = json_decode($order->line_items, true);
                $ids_to_sync = array();
                foreach ($wc_products as $wc_product) {
                    #echo $wc_product['product_id'];
                    //Test Order? 
                    $product_id = $wc_product['product_id'];
                    if ($product_id == '83489' || $product_id == '84093') {
                        $order->is_test_order = true;
                    }
                    $order->save();

                    //Sync Products
                    $product = product::where('woocommerce_id', $wc_product['product_id'])->first();
                    if (!is_null($product)) {
                        //Find Variation ID
                        $variation_id = 0; #If no variation
                        if($wc_product['variation_id']>0) {
                            $variation = variation::where('woocommerce_id',$wc_product['variation_id'])->first();
                            if(!is_null($variation)) {
                                $variation_id = $variation->id;
                            }
                        }
                        //Quantity
                        $quantity = $wc_product['quantity'];

                        //Attach product, variation and quantity (create entry in pivot table) (IF the entry is not already there yet...)
                        if(! $order->products()->where('products.id', $product->id)->exists()) {
                        //if(! $order->products()->contains($wc_product['product_id'])) {
                            $order->products()->attach($product->id,['variation_id'=>$variation_id,'quantity'=>$quantity]);
                        }
                    } 
                    else {
                        //Add a non_identified product counter to the order -> to be displayed on orderview, that something has not been recognized properly
                        $order->not_identified_products += 1;
                        $order->save();
                    }
                    $order->fresh();
                }
            }
    }
        //Import Completed, return error collection (empty would be great)
        return $errors;
    }
}



?>