<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class organization extends Model
{
    protected $guarded = ['id'];
    public function opportunities() 
    {
        return $this->hasMany('App\opportunity');
    }
    public function deliveries()
    {
        return $this->hasMany('App\delivery');
    }
}
