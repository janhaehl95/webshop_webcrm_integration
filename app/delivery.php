<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class delivery extends Model
{
    protected $guarded = ['id'];
    public function organization()
    {
        return $this->belongsTo('App\organization');
    }
}
