<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $guarded = ['id'];
    public function products() {
        return $this->belongsToMany('App\product')->withPivot('variation_id','quantity');
    }

    public function linkeditems() {
        return $this->belongsToMany('App\linkeditem');
    }

    public function variations() {
        return $this->belongsToMany('App\product')->withPivot('variation_id');
    }

    public function organization() {
        return $this->belongsTo('App\organization');
    }
    public function opportunity() {
        return $this->belongsTo('App\opportunity');
    }
    public function person() {
        return $this->belongsTo('App\person');
    }
    public function mca() {
        return $this->belongsTo('App\mca');
    }
}
