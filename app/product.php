<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $guarded = ['id'];
    public function orders() {
        return $this->belongsToMany('App\order');
    }

    public function variations() {
        return $this->hasMany('App\variation');
    }
}
