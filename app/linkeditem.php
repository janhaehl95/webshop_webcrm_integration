<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class linkeditem extends Model
{
    protected $guarded = ['id'];

    public function orders() {
        return $this->belongsToMany('App\order');
    }
}
