<?php

namespace App\Http\Controllers;

use App\linkeditem;
use App\order;
use Illuminate\Http\Request;

class LinkeditemController extends Controller
{
#Test
    /**
     * connect()
     * Connects linkeditem with order -> creates quotation line item
     * @param $order to identify the order & request to take the selected linkeddataitems
     * @return back to view with success
     */

    public function sync($order_id, Request $request) {
        //Validate request
        $request->validate([
            'linkeddataselect.*'=>['integer','exists:linkeditems,id']
        ]);

        //Find order and make sure that its an existing record
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','The order could not be found.');
        }

        //return $request->linkeddataselect;

        //TO-DO: Also validate if it has already been synced with webCRM
        //XXX
        
        //Sync the order
        $order->linkeditems()->sync($request->linkeddataselect);

        //Return back
        return back()->with('success','Quotation Lines Updated');
    }

    public function remove($order_id,$item_id) {
        //Find order & item
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }
        $item = linkeditem::find($item_id);
        if(is_null($item)) {
            return back()->with('error','LinkedItem could not be found.');
        }

        $order->linkeditems()->detach($item->id);

        return back()->with('success','The line has been removed.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\linkeditem  $linkeditem
     * @return \Illuminate\Http\Response
     */
    public function show(linkeditem $linkeditem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\linkeditem  $linkeditem
     * @return \Illuminate\Http\Response
     */
    public function edit(linkeditem $linkeditem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\linkeditem  $linkeditem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, linkeditem $linkeditem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\linkeditem  $linkeditem
     * @return \Illuminate\Http\Response
     */
    public function destroy(linkeditem $linkeditem)
    {
        //
    }
}
