<?php

namespace App\Http\Controllers;
use App\Http\Controllers\webcrmcontroller;
use App\organization;
use Carbon\carbon;
use App\order;
use App\product;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use DB;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizations = organization::paginate();

        return view('organizations.index',compact('organizations'));
    }

    public function import()
    {
        $client = new Client();
        $token = webcrmcontroller::getToken();
        $headers = [
            'Authorization' => 'Bearer ' . $token,        
        ];
        
        try{
            /**
             * We need to iterate through pages, per page -> 50
             * So we need to iterate until the response is empty (empty body)
             * $x is the iterator
             */
            $x = 1;
            $loop_active = true;
            while($loop_active) {
                $request = $client->request('GET','https://api.webcrm.com/Organisations?page='.$x,[
                    'headers' => $headers
                ]);
                //Get Response
                $response = json_decode($request->getBody(),true);

                //Is response empty?
                if(empty($response)) {
                    //This page is empty (empty response array), so we need to escape the while loop
                    break; 
                }
                //The response is not empty, so we will import / update the organization
                foreach ($response as $r) {
                    //Is there already an organization?
                    
                    $organization = organization::firstOrCreate(['webcrm_id' => $r['OrganisationId']]);

                    $organization->city = $r['OrganisationCity'];
                    $organization->country = $r['OrganisationCountry'];
                    $organization->address = $r['OrganisationAddress'];
                    $organization->postcode = $r['OrganisationPostCode'];
                    $organization->created_at = Carbon::parse($r['OrganisationCreatedAt']);
                    $organization->updated_at = Carbon::now();
                    $organization->created_by = $r['OrganisationCreatedBy'];
                    $organization->division_name = $r['OrganisationDivisionName'];
                    $organization->customer_number = $r['OrganisationExtraCustom2'];
                    $organization->webcrm_id = $r['OrganisationId'];
                    $organization->name = $r['OrganisationName'];
                    $organization->status = $r['OrganisationStatus'];
                    $organization->phone = $r['OrganisationTelephone'];
                    $organization->url = $r['OrganisationWww'];
                    $organization->email_invoice = $r['OrganisationExtraCustom3'];
                    $organization->email_cardiomatics = $r['OrganisationCustom2'];
                    $organization->email_contact = $r['OrganisationCustom6'];
                    $organization->save();
                }

                //Update iterator for next
                $x++;
            }
        }
        catch (RequestException $e) {
            return $e->getMessage();
        }

        return back()->with('success','Organizations imported / updated');
    }

    public function search_organisation(Request $request) {
        if($request->ajax())
        {
            $output = '';
            $query = $request->get('query');
            $order_id = $request->get('order_id');
            if($query != '')
            {
                $data = DB::table('organizations')
                    ->where('name','like','%'.$query.'%')
                    ->orWhere('division_name','like','%'.$query.'%')
                    ->orWhere('postcode','like','%'.$query.'%')
                    ->orWhere('address','like','%'.$query.'%')
                    ->orWhere('phone','like','%'.$query.'%')
                    ->orWhere('city','like','%'.$query.'%')
                    ->orWhere('email_invoice','like','%'.$query.'%')
                    ->orWhere('email_cardiomatics','like','%'.$query.'%')
                    ->limit(50)
                    ->get();
            }
            else
            {
                $data = DB::table('organizations')
                    ->limit(0)
                    ->get();
            }
            $total_row = $data->count();
            if($total_row > 0)
            {
                foreach($data as $row)
                {
                    $output .= '
                        <tr>
                            <td>'.$row->name.'</td>
                            <td>'.$row->division_name.'</td>
                            <td>'.$row->customer_number.'</td>
                            <td>'.$row->city.'</td>
                            <td>'.$row->email_invoice.'</td>
                            <td>'.$row->email_cardiomatics.'</td>
                            <td><a role="button" class="btn btn-outline-primary btn-sm" target="_blank" title="Open in WebCRM (new Tab)" href="https://v5.b2bsys.net/MainMenu/Organisations/OrganisationCard/OrganisationCard.aspx?orgId='.$row->webcrm_id.'"><img
                            src="/images/crm_logo.png" class="img-fluid" alt="webCRM"
                            width="100px;"></a></td>
                            <td><a role="button" class="btn btn-primary btn-sm" title="Choose this organization" href="/order/'.$order_id.'/sync/organization/'.$row->id.'" >Continue</a></td>
                        </tr>
                    ';
                }
            }
            else
            {
                $output = '
                    <tr>
                        <td align="center" colspan="8">No results.</td>
                    </tr>
                ';
            }
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row
            );

            echo json_encode($data);
        }
    }

    

   public function order_connect(request $request) {
    //Connects
   }
}
