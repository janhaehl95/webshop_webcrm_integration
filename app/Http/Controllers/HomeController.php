<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\setting;
use App\product;
use App\order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $orders = order::orderBy('id','desc')->paginate(10);
        $filter = array();
        return view('home', compact('orders', 'filter'));
    }

            /**
     * Search / Browse Homescreen
     */
    public function browse(request $request)
    {
        //Validate
        $request->validate([
            'search' => 'max:255',
            'sort' => 'in:order_id,name,company',
            'perpage' => 'integer|min:5|max:100'
        ]);

        //Create Filter Array
        $filter = array();

        $search_query = $request->get('search');
        $filter['search'] = $search_query;
        //Sort
        $sort = "";
        switch ($request->get('sort')) {
            case 'order_id':
                $sort = 'order_id';
                break;
            case 'name':
                $sort = 'billing_last_name';
                break;
            case 'company':
                $sort = 'billing_company';
                break;
            default:
                $sort = 'order_id';
        }
        $filter['sort'] = $request->get('sort');
        //Per Page
        $per_page = 10; //Default
        $per_page = (int) $request->get('perpage');
        $filter['perpage'] = $per_page;

        //Query
        $orders = order::when($request->get('search'), function ($query) use ($search_query) {
            return $query->where('order_id', 'LIKE', '%' . $search_query . '%')
                ->orWhere('billing_first_name', 'LIKE', '%' . $search_query . '%')
                ->orWhere('billing_last_name', 'LIKE', '%' . $search_query . '%')
                ->orWhere('title', 'LIKE', '%' . $search_query . '%')
                ->orWhere('billing_company', 'LIKE', '%' . $search_query . '%');
        })
            ->when($request->get('sort'), function ($query) use ($sort) {
                return $query->orderBy($sort);
            })
            ->paginate($per_page);



        return view('home', compact('orders', 'filter'));
    }

    /**
     * General function for import & update stuff from the webshop
     * Order: Products - Variations - Orders
     * All the individual import functions are stored in app/import.php
     * The individual functions create an array of errors and return them
     * If the import is successful, an empty array is returned.
     * Error messages are thrown back to the user after every step, 
     * so if the process is not successful, it will not continue.
     * This is because variations have relations to products and 
     * orders have relations to products.
     * 
     * @param none
     * @return back() with message
     */
    public function import() {
        //Error Collection
        $errors = collect();
        
        //Products
        #Skipping that for now
        /*
        $product_import = import_products();
        if(!empty($product_import)) {
            foreach($product_import as $product_error) {
                $errors->add($product_error."...");
            }
            $errors->add("Aborted because products could not be imported or updated. Please forward this message to Jan.");
            return back()->withErrors($errors);
        }
        

        //Variations
        $products = product::all();
        foreach($products as $product) {
            $variations_import = import_variations($product);
            if(!empty($variations_import)) {
                foreach($variations_import as $variation_error) {
                    $errors->add($variation_error."...");
                }
                $product_error_string = $product->name." (ID ".$product->id.")";
                $errors->add("Aborted because variation import failed for product ".$product_error_string.". Please forward this message to Jan.");
                return back()->withErrors($errors);
            }
        }
        */

        //Orders
        $orders_import = import_orders();
        if(!empty($orders_import)) {
            foreach($orders_import as $order_error) {
                $errors->add($order_error."...");
                $errors->add("Aborted because orders could not be imported or updated. Please forward this message to Jan.");
                return back()->withErrors($errors);
            }
        }

        //Assume that the Webshop import is complete
        return back()->with('success','Webshop Import/Update completed successfully.');
    }

    public function import_webcrm() {
        $errors = collect();
        //Organization
        $organization_import = import_organizations();
        if(!empty($organization_import)) {
            foreach($organization_import as $organization_error) {
                $errors->add($organization_error."...");
            }
            $errors->add("Aborted because Organizations could not be imported or updated. Please forward this message to Jan.");
            return back()->withErrors($errors);
        }
        //LinkedData
        $linkeddata_import = import_linkeddata();
        if(!empty($linkeddata_import)) {
            foreach($linkeddata_import as $linkeddata_error) {
                $errors->add($linkeddata_error."...");
            }
            $errors->add("Aborted because Linked Data Items could not be imported or updated. Please forward this message to Jan.");
            return back()->withErrors($errors);
        }
        //Opportunities
                $opportunities_import = import_opportunities();
                if(!empty($opportunities_import)) {
                    foreach($opportunities_import as $opportunity_error) {
                        $errors->add($opportunity_error."...");
                    }
                    $errors->add("Aborted because Opportunities could not be imported or updated. Please forward this message to Jan.");
                    return back()->withErrors($errors);
                }
        //Deliveries
        $deliveries_import = import_deliveries();
        if(!empty($deliveries_import)) {
            foreach($deliveries_import as $delivery_error) {
                $errors->add($delivery_error."...");
            }
            $errors->add("Aborted because Deliveries could not be imported or updated. Please forward this message to Jan.");
            return back()->withErrors($errors);
        }

        //Import Complete
        return back()->with('success','WebCRM Data imported / updated.');
    }


    /**
     * create_credentials
     * create webCRM token (valid for 3600 s)
     */
    public function create_credentials() {

        $headers = [
            'authCode' => 'd724fd19-257a-4baa-a6e7-b8fbcdb157eb'
        ];

        $client = new Client();
        try {
            $response = $client->request('POST','https://api.webcrm.com/auth/apilogin',[
                'headers' => $headers,

            ]);
            $status = $response->getStatusCode();
        }
        catch (RequestException $e) {
            return $e->getMessage();
        }

        $content = json_decode($response->getBody(),true);
        $token = $content['AccessToken'];

        $setting = new setting();
        $setting->apikey = $token;
        $setting->save();
        
        return back()->with('success','WebCRM AccessToken generated and stored.');
    }
}