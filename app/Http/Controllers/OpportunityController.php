<?php

namespace App\Http\Controllers;

use App\opportunity;
use Illuminate\Http\Request;

class OpportunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function show(opportunity $opportunity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function edit(opportunity $opportunity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, opportunity $opportunity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\opportunity  $opportunity
     * @return \Illuminate\Http\Response
     */
    public function destroy(opportunity $opportunity)
    {
        //
    }
}
