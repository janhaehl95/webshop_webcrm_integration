<?php

namespace App\Http\Controllers;

use App\mca;
use App\order;
use Illuminate\Http\Request;

class McaController extends Controller
{
    public function connect($order_id,$mca_id) {

        //Find order & MCA
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }
        $mca = mca::find($mca_id);
        if(is_null($mca)) {
            return back()->with('error','MCA could not be found.');
        }
        if($order->is_synced) {
            return back()->with('error','The order is already synced.');
        }

        //Connect
        $order->mca_id = $mca_id;
        $order->save();

        return back()->with('success','MCA connected successfully.');
    }

    public function disconnect($order_id) {

        $order = order::find($order_id);
        if($order->is_synced) {
            return back()->with('error','The order is already synced.');
        }
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }

        $order->mca_id = null;
        $order->save();

        return back()->with('success','MCA disconnected.');
    }

    public function insert()
    {
        $mca = mca::create(
            [
            'webcrm_number' => "01",
            'name' => 'Jörg Brüchmann',
            'email' => 'joerg.bruechmann@pfizer.com',
            'coupon_code'=> '2001'
            ]);
        $mca = mca::create(
            [
                'webcrm_number' => "02",
                'name' => 'Kathrin Lütgert',
                'email' => 'Kathrin.Luetgert@pfizer.com',
                'coupon_code'=> '2002'
            ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "03",
                    'name' => 'Uta Budde',
                    'email' => 'Uta.Budde@pfizer.com',
                    'coupon_code'=> '2003'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "04",
                    'name' => 'Marcus Fink',
                    'email' => 'Marcus.Fink@Pfizer.com',
                    'coupon_code'=> '2004'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "05",
                    'name' => 'Sabine Fuhrmann',
                    'email' => 'Sabine.Fuhrmann@Pfizer.com',
                    'coupon_code'=> '2005'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "06",
                    'name' => 'Sabine Müller-Ströbel',
                    'email' => 'Sabine.Mueller-Stroebel@pfizer.com',
                    'coupon_code'=> '2006'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "07",
                    'name' => 'Claudia Schermer',
                    'email' => 'claudia.schermer@pfizer.com',
                    'coupon_code'=> '2007'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "08",
                    'name' => 'Ines Thiele',
                    'email' => 'Ines.Thiele@pfizer.com',
                    'coupon_code'=> '2027'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "09",
                    'name' => 'Martina Krüsch',
                    'email' => 'Martina.Kruesch@pfizer.com',
                    'coupon_code'=> '2101'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "10",
                    'name' => 'Christoph Meier',
                    'email' => 'Christoph.Meier@pfizer.com',
                    'coupon_code'=> '2102'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "11",
                    'name' => 'Martin Bauerfeind',
                    'email' => 'Martin.Bauerfeind@pfizer.com',
                    'coupon_code'=> '2103'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "12",
                    'name' => 'Heike Stolzis',
                    'email' => 'Heike.Stolzis@pfizer.com',
                    'coupon_code'=> '2104'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "13",
                    'name' => 'Michael Coenen',
                    'email' => 'Michael.Coenen@pfizer.com',
                    'coupon_code'=> '2105'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "14",
                    'name' => 'Andrea Clemens-Votteler',
                    'email' => 'Andrea.Clemens@Pfizer.com',
                    'coupon_code'=> '2106'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "15",
                    'name' => 'Alexandra Syring-Ehemann',
                    'email' => 'Alexandra.Syring-Ehemann@Pfizer.com',
                    'coupon_code'=> '2107'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "16",
                    'name' => 'Florian Kramer',
                    'email' => 'Florian.Kramer@Pfizer.com',
                    'coupon_code'=> '2108'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "17",
                    'name' => 'Bernd Ganser',
                    'email' => 'Bernd.Ganser@Pfizer.com',
                    'coupon_code'=> '2199'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "18",
                    'name' => 'Frank Kieschnick',
                    'email' => 'Frank.Kieschnick@Pfizer.com',
                    'coupon_code'=> '2201'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "19",
                    'name' => 'Ricarda Hegmann-Lieber',
                    'email' => 'Ricarda.Hegmann-Lieber@Pfizer.com',
                    'coupon_code'=> '2202'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "20",
                    'name' => 'Martin Schneidereit',
                    'email' => 'Martin.Schneidereit@pfizer.com',
                    'coupon_code'=> '2203'
                ]);
            $mca = mca::create(
                [
                    'webcrm_number' => "21",
                    'name' => 'Christine Ehrenfels',
                    'email' => 'Christine.Ehrenfels@pfizer.com',
                    'coupon_code'=> '2204'
                ]);
                $mca = mca::create(
                [
                    'webcrm_number' => "22",
                    'name' => 'Thomas Schömig',
                    'email' => 'Thomas.Schoemig@pfizer.com',
                    'coupon_code'=> '2205'
                ]);
                $mca = mca::create(
                [
                    'webcrm_number' => "23",
                    'name' => 'Tanja Stidinger',
                    'email' => 'Tanja.Stidinger@pfizer.com',
                    'coupon_code'=> '2206'
                ]);
                $mca = mca::create(
                [
                    'webcrm_number' => "24",
                    'name' => 'Carmen Wissenbach',
                    'email' => 'Carmen.Wissenbach@pfizer.com',
                    'coupon_code'=> '2207'
                ]);
                $mca = mca::create(
                [
                    'webcrm_number' => "25",
                    'name' => 'Christiane Kotz',
                    'email' => 'Christiane.Kotz@pfizer.com',
                    'coupon_code'=> '2227'
                ]);
                $mca = mca::create(
                [
                    'webcrm_number' => "26",
                    'name' => 'Helmut Baumgärtner',
                    'email' => 'Helmut.Baumgaertner@pfizer.com',
                    'coupon_code'=> '2299'
                ]);

    }
}
