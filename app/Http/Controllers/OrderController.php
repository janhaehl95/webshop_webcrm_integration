<?php

namespace App\Http\Controllers;

use App\order;
use App\mca;
use App\product;
use App\linkeditem;
use App\organization;
use App\opportunity;
use App\variation;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

use DB;

class OrderController extends Controller
{

    /**
     * This central view function will return the right view, depending on the situation.
     * 
     * First step -> Will present order and selection to choose organization
     */
    public function view($order_id)
    {
        //Find Order
        $order = order::find($order_id);
        if (is_null($order)) {
            return back()->with('error', 'This order could not be found.');
        }

        //Related orders
        $order->related_orders = order::where('customer_id', $order->customer_id)
        ->orderBy('ordered_at', 'asc')
        ->get();

        if($order->is_synced) {
            return view('orderviews.04_synced', compact('order'))->with('error','The order has been synced. To change it, go to webCRM.');
        }

        /**
         * Step 1 - Choose Organization
         */
        if(is_null($order->organization_id)) {
            return view('orderviews.01_organization',compact('order'));
        }
        /**
         * Step 2 - Choose Opportunity
         */
        
        elseif(is_null($order->opportunity_id) && !is_null($order->organization_id)) {
            return view('orderviews.02_opportunity',compact('order'));
        }

        /**
         * Step 3 - Choose Quotation Lines
         */
        elseif(!is_null($order->opportunity_id) && !is_null($order->organization_id)) {
            //Get linkeddata
            $linkeddata = linkeditem::all();
            $mcas = mca::all();
            return view('orderviews.03_quotationlines',compact('order','linkeddata','mcas'));
        }

         /**
          * Step 5 - Catch
          */
        else {
            return redirect()->route('home')->with('error','There has been an error in connecting the order to webCRM data. Please try again or contact Jan.');
        }


        //Add Variation to product name
        foreach($order->products as $product) {
        $variation_id = $product->pivot->variation_id;
            if($variation_id > 0) {
                $variation = variation::find($variation_id);
                $product->variation = $variation->option;
            }
        }



        return view('orderviews.01_organization', compact('order'));

    }

    /**
     * STEP 1 
     */
    public function assign_organisation($order_id,$organization_id)
    {
        //Find Order & Organization
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }
        $organization = organization::find($organization_id);
        if(is_null($organization)) {
            return back()->with('error','Organization could not be found.');
        }

        // Make sure that the order is not synced yet
        if($order->is_synced) {
            return back()->with('error','The order is already synced.');
        }

        //Update Order
        $order->organization_id = $organization_id;
        $order->save();

        //Return
        return back()->with('success','Connected to the organization.');
    }

    /**
     * STEP 1
     * new_organisation
     * Puts 0 in organization_id -> tells the sync to create a new organization.
     */
    public function new_organisation($order_id)
    {
        //Find Order
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }

        //Set org to 0
        $order->organization_id = 0;
        //Also set opportunity to 0, as no existing opportunities for new organizations
        $order->opportunity_id = 0;
        $order->save();

        //Return
        return back()->with('success','A new organization will be created upon synchronisation.');

    }

    /**
     * STEP 1
     */
    public function disconnect_organisation($order_id)
    {
        //Find Order
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }

        $order->organization_id = null;
        $order->opportunity_id = null;
        $order->save();

        return back()->with('success','The organization has been disconnected.');
    }

    /**
     * STEP 2
     */
    public function assign_opportunity($order_id,$opportunity_id)
    {
        //Find Order & Organization
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }
        $organization = opportunity::find($opportunity_id);
        if(is_null($organization)) {
            return back()->with('error','Opportunity could not be found.');
        }

        // Make sure that the order is not synced yet
        if($order->is_synced) {
            return back()->with('error','The order is already synced.');
        }

        //Update Order
        $order->opportunity_id = $opportunity_id;
        $order->save();
        return back()->with('success','Conntected to the opportunity.');
    }

    public function new_opportunity($order_id)
    {
        //Find Order
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }

        //Set org to 0
        $order->opportunity_id = 0;
        //Also set opportunity to 0, as no existing opportunities for new organizations
        $order->save();

        //Return
        return back()->with('success','A new opportunity will be created upon synchronisation.');

    }

    public function disconnect_opportunity($order_id)
    {
        //Find Order
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->with('error','Order could not be found.');
        }

        $order->opportunity_id = null;
        #$order->linkeditems->detach(); #Detach linkedItems if already connected..
        $order->save();

        return back()->with('success','The opportunity has been disconnected.');
    }



    public function view_old($order_id)
    {
        $order = order::find($order_id);
        if (is_null($order)) {
            return back()->with('error', 'This order could not be found.');
        }

        //Add Variation to product name
        foreach($order->products as $product) {
            $variation_id = $product->pivot->variation_id;
            if($variation_id > 0) {
                $variation = variation::find($variation_id);
                $product->variation = $variation->option;
            }
        }

        /**
         * Nice presentation of products (line_items)
         */
        //$order->products = json_decode($order->line_items,true); 

        /**
         * Other orders?
         */
        $order->related_orders = order::where('customer_id', $order->customer_id)
            ->orderBy('ordered_at', 'asc')
            ->get();
            
        //Get LinkedData Items
        $linkeddata = linkeditem::all();

        return view('orderview', compact('order','linkeddata'));
    }


    
}