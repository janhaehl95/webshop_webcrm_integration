<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\product;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class ProductController extends Controller
{
    public function index() {
        $products = product::all();

        return view('products.index', compact('products'));
    }

    /**
     * Returns the orders that are associated with a product
     */
    public function orderview($product_id) {
        $product = product::find($product_id);
        $orders = order::whereHas('products', function ($query) use ($product) {
            return $query->where('id',$product->id);
        })->get();

        return view('products.orderview', compact('orders','product'));
    }

    public static function import() {
        $client = new Client();
        try {
            $req = $client->get("https://cortrium.com/wp-json/wc/v3/products?per_page=100&lang=all", ['auth' => ['ck_79c050882fc32202cab85d2026b9a9ac25e66db8', 'cs_21c9b8f5bf6af415f34d35a42b7dee494f268258']]);
            $status = $req->getStatusCode();

            $response = json_decode($req->getBody(),true);

            //Loop through response and add products
            foreach($response as $r) {
                $product = product::firstOrCreate(['woocommerce_id' => $r['id']],
                [
                    'name' => $r['name'],
                    'lang' => $r['lang']
                ]
                );
                

                //Import variants
                $product->fresh(); #Refresh from db
                import_variations($product);
            }
        }
        catch(RequestException $e) {
            return $e->getMessage();
        }

        return true;
    }
}
