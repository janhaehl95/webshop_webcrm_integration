<?php

namespace App\Http\Controllers;

use App\order;
use App\organization;
use App\opportunity;
use Carbon\carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\setting;

class webcrmcontroller extends Controller
{

    public function sync_order($order_id)
    {
        //Create error collection
        $errors = collect();

        //Validate Order
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->withError('The order could not be found. Aborting synchronization.');
        }

        //Ready for sync?
        if($order->is_synced) {
            return back()->withError('Can not sync the order, it is already synced.');
        }
        if(is_null($order->organization_id)) {
            return back()->withError('Can not sync the order, it is not associated with an organization.');
        }
        if(is_null($order->opportunity_id)) {
            return back()->withError('Can not sync the order, it is not associated with an opportunity.');
        }

        /**
         * Sync Organization
         * When an existing webCRM organization is associated with the order, nothing is done to the organization on webCRM.
         * Else, a new organization is created.
         */
        $organization = null;
        if($order->organization_id == 0) {
            $organization_push = create_organization($order);
            if(!empty($organization_push)) {
                foreach($organization_push as $organization_error) {
                    $errors->add($organization_error."...");
                }
                
                $errors->add("Organization PUSH error. Please forward this message to Jan.");
                return back()->withErrors($errors);
            }
        }
        $organization = organization::find($order->organization_id);
        $organization->fresh();

        /**
         * Sync Opportunity
         * When an existing opportunity is associated with the order, it is updated.
         * Otherwise, a new opportunity is created.
         */
        $organization_id = $organization->id;
        if($order->opportunity_id == 0) {
            //New Opportunity
            
            $opportunity_push = create_opportunity($order,$organization);

            if(!empty($opportunity_push)) {
                foreach($opportunity_push as $opportunity_error) {
                    $errors->add($opportunity_error."...");
                }
                
                $errors->add("Opportunity PUSH error. Please forward this message to Jan.");
                return back()->withErrors($errors);
            }
        }
        else {
            $opportunity = opportunity::find($order->opportunity_id);
            #Update existing opportunity
            $opportunity_put = update_opportunity($opportunity,$order,$organization);
            if(!empty($opportunity_put)) {
                foreach($opportunity_put as $opportunity_error) {
                    $errors->add($opportunity_error."...");
                }
                
                $errors->add("Opportunity PUT error. Please forward this message to Jan.");
                return back()->withErrors($errors);
            }
        }

        #Update the order, to keep fresh!
        $order->fresh();

        /**
         * Create Quotation Lines and associate them with the opportunity
         */
        $opportunity = opportunity::find($order->opportunity_id);
        $quotation_lines_push = create_quotation_lines($opportunity,$order,$organization);
        if(!empty($quotation_lines_push)) {
            foreach($quotation_lines_push as $quotation_line_error) {
                $errors->add($quotation_line_error."...");
            }
            
            $errors->add("Quotation Line PUSH error. Please forward this message to Jan.");
            return back()->withErrors($errors);
        }

        //Set order to synced
        $order->is_synced = true;
        $order->save();
        $order_id = $order->id;

        return redirect()->route('order.view',$order_id)->with('success','The order has been synced.');

    }

    public function sync_order_old($order_id)
    {
        //Validate Order
        $order = order::find($order_id);
        if(is_null($order)) {
            return back()->withError('Order Not Found');
        }

        //Sync Organization
        $webcrm_organisation_id = 0;
        if($order->webcrm_organisation_id) {
            $webcrm_organisation_id = $order->webcrm_organisation_id;
        }
        else {
            $create_organisation = webcrmcontroller::create_organization($order);
            if($create_organisation!="200") {
                return back()->withError($create_organisation);
            }
            $webcrm_organisation_id = $order->webcrm_organisation_id;
             #To make sure that we have space for the opportunity
        }
        $order = order::find($order_id);

        //Sync Person
        $webcrm_person_id = 0;
        if($order->webcrm_person_id) {
            //Person exists
            $webcrm_person_id = $order->webcrm_person_id;
        }
        else {
            //Create a new person
            $create_person = webcrmcontroller::create_person($order);
            if($create_person!="200") {
                return back()->withError($create_person);
            }
        }

        $order = order::find($order_id);
        
        //Create Opportunity & Quotation Lines
        $webcrm_opportunity_id = 0;
        if($order->webcrm_opportunity_id) {
            $webcrm_opportunity_id = $order->webcrm_opportunity_id;
        }
        else {
            //Create Opportunity
            $create_opportunity = webcrmcontroller::create_opportunity($order,$webcrm_organisation_id);
            if($create_opportunity!="200") {
                return back()->withError($create_opportunity);
            }
            $webcrm_opportunity_id = $order->webcrm_opportunity_id;
            #Refresh
            $order = $order::find($order_id);
            //Create Quotation Lines - Waiting answer from Lasse
            $create_quotation = webcrmcontroller::create_quotation($order);
            return $create_quotation;
        }

        //Create QuotationLines


        return back()->with('success','The Sync has been successful.');
        
    }

    public static function create_opportunity_old($order,$webcrm_organisation_id)
    {
        /**
         * Set some values for the import
         */
        $mca_code = "-";
        if($order->mca_code) {
            //Is a Pfizer Customer
            $mca_code = $order->mca_code;
        }
        $pipeline_level = 3;
        $test_before_purchase = "[1]"; //1 -> No, 2 -> Yes, both can be set. Setting Yes for all test versions.
        $pipeline_level_text = "[3] Qualified / Test Order (20%)";
        $discount_text = "Without discount";
        if($order->total>0) {
            //Assuming Test Order
            $pipeline_level = 11;
            $pipeline_level_text = "[11] Order Comfirmed (98%)";
            $test_before_purchase = "[1]";
        }

        if($order->discount_total>0) {
            //Discount
            $discount_text = "With discount";
        }

        #$token = env('api_token');
        $token = webcrmcontroller::getToken();
        $headers = [
            'Authorization' => 'Bearer ' . $token,        
            'Content-Type'        => 'application/json',
        ];
        $client = new Client();
    

        try {
            $response = $client->request('POST','https://api.webcrm.com/Opportunities',[
                'headers' => $headers,
                'body' => json_encode([
                    'OpportunityAssignedTo' => 7,
                    'OpportunityAssignedTo2' => 6,
                    'OpportunityComment' =>  'MCA Code: '.$mca_code,
                    'OpportunityCreatedBy' => 'Webshop',
                    'OpportunityOrderDate' => $order->ordered_at,
                    'OpportunityCurrencyName' => '03',
                    'OpportunityCurrencySymbol' => 'EUR',
                    'OpportunityDescription' => 'Test',
                    'OpportunityDiscount' => $order->discount_total,
                    'OpportunityId' => 0,
                    'OpportunityLevel' => $pipeline_level,
                    'OpportunityLevelText' => $pipeline_level_text,
                    'OpportunityCustom1' => $test_before_purchase, //Test before purchase No / Yes
                    'OpportunityCustom7' => $discount_text,
                    'OpportunityOrganisationId' => $order->webcrm_organisation_id,
                ])
            ]);
            $status = $response->getStatusCode();
        }
        catch(RequestException $e) {
            return $e->getMessage();
        }

        $webcrm_opp_id = (int) json_decode($response->getBody());
        $order->webcrm_opportunity_id = $webcrm_opp_id;
        $order->save();

        return $status;

    }
    public static function create_organization_old($order) {
        if($order->webcrm_organisation_id) {
            return back()->with('warning','Organization has already been created.');
        }

        /**
         * Set some values for the order
         */
        $lead_from = "00:--Choose--";
        $mca_code = "-";
        if($order->mca_code) {
            //Is a Pfizer Customer
            $lead_from = "08: Pfizer";
            $mca_code = $order->mca_code;
        }

        #$token = env('api_token');
        $token = webcrmcontroller::getToken();
        $client = new Client(['base_uri' => 'https://api.webcrm.com/Organisations']);
        $headers = [
            'Authorization' => 'Bearer ' . $token,        
            'Content-Type'        => 'application/json',
        ];
        $client = new Client();

        try {
            $response = $client->request('POST','https://api.webcrm.com/Organisations',[
                'headers' => $headers,
                'body' => json_encode([
                    'OrganisationAddress' => $order->billing_address_1." ".$order->billing_address_2,
                    'OrganisationAlert' => 'TEST Webshop Import',
                    'OrganisationApprovalStatus' => 0,
                    'OrganisationCity' => $order->billing_city,
                    'OrganisationComment' => 'Webshop Import',
                    'OrganisationCountry' => 'D - Deutschland',
                    'OrganisationDivisionName' => $order->billing_first_name." ".$order->billing_last_name,
                    'OrganisationExtraCustom2' => $order->customer_id,
                    'OrganisationExtraCustom6' => 'EUR',
                    'OrganisationExtraCustom8' => $lead_from,
                    'OrganisationId' => 0,
                    'OrganisationName' => 'TEST'.$order->billing_company,
                    'OrganisationOwner' => 6,
                    'OrganisationOwner2' => 7,
                    'OrganisationPostCode' => $order->billing_postcode,
                    #'OrganisationTelephone' => $order->phone,
                    'OrganisationCustom1' => '00:--Choose--',
                    'OrganisationCustom2' => $order->cardiomatics_email,
                    'OrganisationCustom6' => $order->email,
                    'OrganisationCustom8' => '08: Pfizer',
                    'OrganisationCustom9' => '[1]',
                    'OrganisationCustom10' => '00:--Choose--\t',
                    'OrganisationCustom11' => '00:--Choose--\t'
                ])
            ]);
            $status = $response->getStatusCode();
        }
        catch(RequestException $e) {
            return $e->getMessage();
        }
        $webcrm_org_id = (int) json_decode($response->getBody());
        //Write this organization id to all orders
        $orders_to_change = order::where('customer_id',$order->customer_id)->get();
        foreach($orders_to_change as $o) {
            $o->webcrm_organisation_id = $webcrm_org_id;
            $o->save();
        }
        return $status;
    }

    public static function create_person($order) {
        if($order->webcrm_person_id) {
            return "Person already exists...";
        }
        if(!$order->webcrm_organisation_id) {
            return "No organisation specified for person...";
        }

        #$token = env('api_token');
        $token = webcrmcontroller::getToken();
        $client = new Client(['base_uri' => 'https://api.webcrm.com/Organisations']);
        $headers = [
            'Authorization' => 'Bearer ' . $token,        
            'Content-Type'        => 'application/json',
        ];
        $client = new Client();

        try {
            $response = $client->request('POST','https://api.webcrm.com/Persons',[
                'headers' => $headers,
                'body' => json_encode([
                    'PersonCreatedAt' => $order->ordered_at,
                    'PersonCreatedBy' => 'Webshop',
                    'PersonEmail' => $order->email,
                    'PersonFirstName' => $order->billing_first_name,
                    'PersonMobilePhone' => $order->phone,
                    'PersonId' => 0,
                    'PersonLastName' => $order->billing_last_name,
                    'PersonName' => $order->billing_first_name.' '.$order->billing_last_name,
                    'PersonOrganisationId' => $order->webcrm_organisation_id,
                    'PersonSalutation' => $order->salutation,
                    'PersonStatus' => 'Active',
                ])
            ]);
            $status = $response->getStatusCode();
        }
        catch (RequestException $e) {
            return $e->getMessage();
        }
        $webcrm_person_id = (int) json_decode($response->getBody());
        //Write Person ID to order
        $order->webcrm_person_id = $webcrm_person_id;
        $order->save();

        return $status;
    }

    public static function create_quotation_linked_data($order) {
        $token = webcrmcontroller::getToken();

    }

    public static function create_quotation($order) {

        if(!$order->webcrm_opportunity_id) {
            return "No Opportunity found while creating quotation...";
        }

        //Get Products from Order
        $products = json_decode($order->line_items,true);

        $opportunity_id = $order->webshop_opportunity_id;
        $organisation_id = $order->webshop_organisation_id;

        $token = webcrmcontroller::getToken();
        $headers = [
            'Authorization' => 'Bearer ' . $token,        
            'Content-Type'        => 'application/json',
        ];
        

        $quotation_count = 0;
        foreach($products as $product) {

            //Identify LinkedData ID
            return webcrmcontroller::getLinkedDataId($product);

            $client = new Client();
            $price = $product['price'];
            $line_memo = $product['name'];
            $quantity = $product['quantity'];
            try {
                $response = $client->request('POST','https://api.webcrm.com/QuotationLines',[
                    'headers' => $headers,
                    'body' => json_encode([
                        'QuotationLineComment' => 'Test Webshop Import',
                        'QuotationLineCostPrice' => 0.0,
                        'QuotationLineCreatedAt' => $order->ordered_at,
                        'QuotationLineCreatedBy' => 'Webshop',
                        'QuotationLineId' => 0,
                        'QuotationLineListPrice' => $price,
                        'QuotationLineMemo' => $line_memo,
                        'QuotationLineOpportunityId' => $opportunity_id,
                        'QuotationLineOrganisationId' => $organisation_id,
                        'QuotationLinePersonId' => 0,
                        'QuotationLinePrice' => $price,
                        'QuotationLineQuantity' => $quantity,
                        'QuotationLineVatPercentage' => 25.0,
                    ])
                ]);
                $status = $response->getStatusCode();
            }
            catch (RequestException $e) {
                return $e->getMessage();
            }
            $quotation_count++;
        }

        //Quotation Line for Discount?
        if($order->discount_total>0) {
            try {
                $response = $client->request('POST','QuotationLines',[
                    'headers' => $headers,
                    'body' => json_encode([
                        'QuotationLineComment' => 'Test Webshop Import',
                        'QuotationLineCreatedAt' => $order->ordered_at,
                        'QuotationLineCreatedBy' => 'Webshop',
                        'QuotationLineId' => 0,
                        'QuotationLineListPrice' => -400,
                        'QuotationLineMemo' => $line_memo,
                        'QuotationLineOpportunityId' => $opportunity_id,
                        'QuotationLineOrganisationId' => $organisation_id,
                        'QuotationLinePersonId' => 0,
                        'QuotationLinePrice' => -400,
                        #Not sooo nice, but for now ok as long as we don't give any other discount than 400
                        'QuotationLineQuantity' => (int)$order->discount_total/400,
                    ])
                ]);
                $status = $response->getStatusCode();
            }
            catch (RequestException $e) {
                return $e->getMessage();
            }
        }
        return $status;
    }

    public static function getToken() {
        $setting = setting::latest()->first();
        $token = $setting->apikey;

        if(is_null($token)) {
            return back()->with('Could not find a token. Please refresh the CRM Connection.');
        }
        return $token;
    }

    /**
     * Returns the linked data ID for a $product,
     * whereas $product is from the json response from the webshop
     * (array element, see above)
     */
    public static function getLinkedDataId($product) {
        return $product;
    }
}
