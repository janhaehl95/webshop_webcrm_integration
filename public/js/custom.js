/**
 * This script contains custom vanilla javascript functions
 */

function searchTable(sourceElement, targetTable) {
    //Get search value
    var query = document.getElementById(sourceElement).value;
    //Get Table
    var target = document.getElementById(targetTable);
    //Get rows 
    var rows = target.rows;
    //Iterate through each row [start at x=1, exclude header row!]
    for(var x=1;x<rows.length;x++) {
        var cells = rows[x].cells;
        var treffer = 0;
        //Iterate through all cells in a row (Start bei 1, da ID nicht betrachtet werden soll, finish bei length-1 da zeitstempel uninteressant)
        for(var y=1;y<cells.length;y++) {
            //Match?
            if(cells[y].innerHTML.toLowerCase().indexOf(query.toLowerCase()) > -1) {
                treffer++;
            }
        }
        //End of row, hide?
        if(treffer<1) {
            rows[x].style.display = 'none';
        }
        else if(treffer>0) {
            rows[x].style.display='';
        }
    }
}