<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('order_id');
            $table->integer('parent_id')->nullable();
            $table->string('order_key')->nullable();
            $table->string('status')->nullable();
            $table->string('currency')->default('EUR');
            $table->dateTime('ordered_at');
            $table->float('discount_total')->default(0);
            $table->float('total')->default(0);
            $table->float('taxes')->default(0);
            $table->integer('customer_id');
            $table->string('customer_ip')->nullable();
            $table->text('customer_user_agent')->nullable();
            $table->text('customer_note')->nullable();
            //Billing Adress
            $table->string('billing_first_name');
            $table->string('billing_last_name');
            $table->string('billing_company');
            $table->string('billing_address_1');
            $table->string('billing_address_2')->nullable();
            $table->string('billing_city');
            $table->string('billing_state')->nullable();
            $table->string('billing_postcode');
            $table->string('billing_country');
            $table->string('email');
            $table->string('phone')->nullable();
            //Shipping Adress
            $table->string('shipping_first_name');
            $table->string('shipping_last_name');
            $table->string('shipping_company');
            $table->string('shipping_address_1');
            $table->string('shipping_address_2')->nullable();
            $table->string('shipping_city');
            $table->string('shipping_state')->nullable();
            $table->string('shipping_postcode');
            $table->string('shipping_country');
            //Payment
            $table->string('payment_method')->nullable();
            $table->string('payment_method_title')->nullable();
            $table->char('transaction')->nullable();
            //META
            $table->string('salutation')->nullable();
            $table->string('title')->nullable();
            $table->string('cardiomatics_email')->nullable();
            $table->string('contact_email')->nullable();
            $table->text('mca_code')->nullable();
            $table->string('language')->nullable(); //Should be en or de... set by wpml
            $table->json('line_items')->nullable();
            
            
            //WEBCRM
            //$table->integer('webcrm_opportunity_id')->nullable();
            //$table->integer('webcrm_organisation_id')->nullable();
            //$table->integer('webcrm_person_id')->nullable();
            //For Product Linking
            $table->integer('not_identified_products')->default(0);

            //WebCRM Linking (new)
            $table->integer('organization_id')->nullable();
            $table->integer('opportunity_id')->nullable();
            $table->integer('person_id')->nullable();
            $table->boolean('is_synced')->default(false);
            $table->integer('mca_id')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
