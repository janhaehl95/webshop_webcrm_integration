<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkeditemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linkeditems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('item_group')->nullable(); #QuotationLineLinkedDataItemItemGroup
            $table->string('economic_item_number')->nullable(); #QuotationLineLinkedDataItemData1
            $table->string('economic_item_description')->nullable(); #QuotationLineLinkedDataItemData2
            $table->text('description')->nullable(); #QuotationLineLinkedDataItemDataMemo
            $table->float('item_price')->default(0); #QuotationLineLinkedDataItemPrice
            $table->integer('webcrm_id'); #QuotationLineLinkedDataItemId
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linkeditems');
    }
}
