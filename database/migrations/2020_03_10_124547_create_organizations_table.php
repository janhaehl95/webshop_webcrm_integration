<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('address')->nullable();
            $table->string('postcode')->nullable();
            $table->string('created_by')->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->string('division_name')->nullable();
            $table->string('customer_number')->nullable(); //OrganisationExtraCustom2
            $table->integer('webcrm_id');
            $table->string('name')->nullable();
            $table->string('status')->nullable();
            $table->string('phone')->nullable();
            $table->string('url')->nullable();
            $table->string('email_invoice')->nullable(); //OrganisationExtraCustom3
            $table->string('email_cardiomatics')->nullable(); //OrganisationCustom2
            $table->string('email_contact')->nullable(); #OrganisationCustom6
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
