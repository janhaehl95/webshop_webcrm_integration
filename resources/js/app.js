/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

function searchTable(sourceElement, targetTable) {
    //Get search value
    var query = document.getElementById(sourceElement).value;
    //Get Table
    var target = document.getElementById(targetTable);
    //Get rows 
    var rows = target.rows;
    //Iterate through each row [start at x=1, exclude header row!]
    for(var x=1;x<rows.length;x++) {
        var cells = rows[x].cells;
        var treffer = 0;
        //Iterate through all cells in a row (Start bei 1, da ID nicht betrachtet werden soll, finish bei length-1 da zeitstempel uninteressant)
        for(var y=1;y<cells.length;y++) {
            //Match?
            if(cells[y].innerHTML.toLowerCase().indexOf(query.toLowerCase()) > -1) {
                treffer++;
            }
        }
        //End of row, hide?
        if(treffer<1) {
            rows[x].style.display = 'none';
        }
        else if(treffer>0) {
            rows[x].style.display='';
        }
    }
}