<?php

use Carbon\Carbon;
?>
@extends('layouts.app')
@section('content')

<div class="container">
  <h1>Orders for {{$product->name}}</h1>
</div>

<div class="container">
  @if(count($orders)>0)
  <div class="row">
    <div class="col-md-12">
      <p>Found {{count($orders)}} orders</p>
      
    </div>
  </div>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Order ID</th>
        <th scope="col">Customer</th>
        <th scope="col">Company</th>
        <th scope="col">Date</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($orders as $order)
      <tr>
        <th scope="row">{{$order->id}}</th>
        <td>{{$order->order_id}}</td>
        <td>{{$order->title}} {{$order->billing_first_name}} {{$order->billing_last_name}}</td>
        <td>{{$order->billing_company?$order->billing_company:'-'}}</td>
        <td>{{Carbon::parse($order->ordered_at)->format('m.d.y, H:i')}}</td>
        <td>
          @if($order->webcrm_organisation_id)
            <a href="https://v5.b2bsys.net/MainMenu/Organisations/OrganisationCard/OrganisationCard.aspx?orgId={{$order->webcrm_organisation_id}}" target="_blank" title="Open WebCRM Organisation"><i class="fas fa-building"></i></a>
          @else
          @endif
          @if($order->webcrm_opportunity_id)
            <a href="https://v5.b2bsys.net/MainMenu/Opportunities/OpportunityView.aspx?action=view&eid={{$order->webcrm_opportunity_id}}" target="_blank" title="Open WebCRM Opportunity"><i class="fab fa-product-hunt"></i></a>
          @else
          @endif
          @if($order->webcrm_person_id)
            <a href="https://v5.b2bsys.net/MainMenu/Persons/PersonView.aspx?ActionType=view&eid={{$order->webcrm_person_id}}" target="_blank" title="Open WebCRM Person"><i class="fas fa-user"></i></a>
          @else
          @endif
        </td>
        <td>
        <a href="{{route('order.view',$order->id)}}" role="button" class="btn btn-outline-primary btn-sm">View</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @endif
</div>

@endsection