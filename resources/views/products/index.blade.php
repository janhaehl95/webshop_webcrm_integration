@extends('layouts.app')
@section('content')

<div class="container">
  <h1>Products</h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
@if(count($products)<1)
<p>No products found.</p>
@else
<p>There are {{count($products)}} products. </p>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Product ID</th>
        <th scope="col">Product Name</th>
        <th scope="col">Language</th>
        <th scope="col">Orders</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($products as $product)
        <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->woocommerce_id}}</td>
            <td>{{$product->name}}</td>
            <td>{{$product->lang}}</td>
            <td>{{count($product->orders)}}</td>
            <td><a role="button" class="btn btn-primary" href="{{route('products.orderview',$product->id)}}">Overview</a></td>
        </tr>
      @endforeach
    </tbody>
</table>
@endif
</div>
</div>
</div>

@endsection