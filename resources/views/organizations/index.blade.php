<?php
use Carbon\carbon;
?>

@extends('layouts.app')
@section('content')

<div class="container">
  <h1>webCRM Organizations</h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
@if(count($organizations)<1)
<p>No organizations found.</p>
@else
<p>There are {{$organizations->total()}} organizations. </p>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">webCRM ID</th>
        <th scope="col">Name</th>
        <th scope="col">Division</th>
        <th scope="col">Created At</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($organizations as $organization)
        <tr>
            <td>{{$organization->id}}</td>
            <td>{{$organization->webcrm_id}}</td>
            <td>{{$organization->name}}</td>
            <td>{{$organization->division_name}}</td>
            <td>{{Carbon::parse($organization->created_at)->format('d.m.Y, H:i')}}</td>
            <td></td>
        </tr>
      @endforeach
    </tbody>
</table>
{{$organizations->links()}}
@endif
</div>
</div>
</div>

@endsection