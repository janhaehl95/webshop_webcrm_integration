@extends('layouts.app')
@section('content')




<div class="container">
<table class="table" id="laravel_datatable">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First Name</th>
      <th scope="col">Created At</th>
    </tr>
  </thead>

</table>

</div>
{{-- Datatable Script --}}
<script>
   $(document).ready( function () {
    $('#laravel_datatable').DataTable({
           processing: true,
           serverSide: true,
           ajax: "{{ url('orders-list') }}",
           columns: [
                    { data: 'id', name: 'id' },
                    { data: 'billing_first_name', name: 'billing_first_name' },
                    { data: 'created_at', name: 'created_at' }
                 ]
        });
     });
  </script>

@endsection