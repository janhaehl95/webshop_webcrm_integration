<?php

use Carbon\Carbon;
?>
@extends('layouts.app')
@section('content')

<div class="container">
  <h1>Cortrium Order Management</h1>
</div>

<div class="container mb-3">
<form method="POST" action="{{route('home.browse')}}">
      @csrf
  <div class="form-row mb-3">
    
      <div class="form-group col-md-4">
        
        <input type="text" id="search" name="search" class="form-control" placeholder="Search" value="@if(isset($filter['search'])){{$filter['search']?$filter['search']:''}}@endif">
        <label for="search"></label>
      </div>
      <div class="form-group col-md-2">
        <label for="sort">Sort</label>
        <select id="sort" name="sort" class="form-control">
          <option value="order_id" @if(isset($filter['sort'])){{$filter['sort']=='order_id'?'selected':''}}@else selected @endif>Order ID</option>
          <option value="name" @if(isset($filter['sort'])){{$filter['sort']=='name'?'selected':''}}@endif>Name</option>
          <option value="company" @if(isset($filter['sort'])){{$filter['sort']=='company'?'selected':''}}@endif>Company</option>
        </select>
      </div>
      <div class="form-group col-md-1">
        <label for="perpage">Per Page</label>
        <select id="perpage" name="perpage" class="form-control">
          <option value="5" @if(isset($filter['perpage'])){{$filter['perpage']==5?'selected':''}}@endif>5</option>
          <option value="10" @if(isset($filter['perpage'])){{$filter['perpage']==10?'selected':''}}@else selected @endif>10</option>
          <option value="25" @if(isset($filter['perpage'])){{$filter['perpage']==25?'selected':''}}@endif>25</option>
          <option value="50" @if(isset($filter['perpage'])){{$filter['perpage']==50?'selected':''}}@endif>50</option>
          <option value="100" @if(isset($filter['perpage'])){{$filter['perpage']==100?'selected':''}}@endif>100</option>
        </select>
      </div>
      {{-- <div class="form-group col-md-2">
        <label for="from">From</label>
        <input id="from" name="from" type="date" class="form-control" placeholder="From">
      </div>
      <div class="form-group col-md-2">
        <label for="to">To</label>
        <input id="to" name="to" type="date" class="form-control" placeholder="To">
      </div> --}}

    
  </div>
  <div class="row">
  <div class="col-md-2">
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
  </div>
  </form>
</div>
<div class="container">
  @if(count($orders)>0)
  <div class="row">
    <div class="col-md-12">
      <p>Found {{$orders->total()}} orders</p>
    </div>
  </div>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Order ID</th>
        <th scope="col">Customer</th>
        <th scope="col">Company</th>
        <th scope="col">Test</th>
        <th scope="col">Date</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($orders as $order)
      <tr>
        <th scope="row">{{$order->id}}</th>
        <td>{{$order->order_id}}</td>
        <td>{{$order->title}} {{$order->billing_first_name}} {{$order->billing_last_name}}</td>
        <td>{{$order->billing_company?$order->billing_company:'-'}}</td>
        <td>{{$order->is_test_order?'Test':'-'}}</td>
        <td>{{Carbon::parse($order->ordered_at)->format('d.m.y, H:i')}}</td>
        <td>
          @if($order->webcrm_organisation_id)
            <a href="https://v5.b2bsys.net/MainMenu/Organisations/OrganisationCard/OrganisationCard.aspx?orgId={{$order->webcrm_organisation_id}}" target="_blank" title="Open WebCRM Organisation"><i class="fas fa-building"></i></a>
          @else
          @endif
          @if($order->webcrm_opportunity_id)
            <a href="https://v5.b2bsys.net/MainMenu/Opportunities/OpportunityView.aspx?action=view&eid={{$order->webcrm_opportunity_id}}" target="_blank" title="Open WebCRM Opportunity"><i class="fab fa-product-hunt"></i></a>
          @else
          @endif
          @if($order->webcrm_person_id)
            <a href="https://v5.b2bsys.net/MainMenu/Persons/PersonView.aspx?ActionType=view&eid={{$order->webcrm_person_id}}" target="_blank" title="Open WebCRM Person"><i class="fas fa-user"></i></a>
          @else
          @endif
        </td>
        <td>
        <a href="{{route('order.view',$order->id)}}" role="button" class="btn btn-outline-primary btn-sm">View</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{ $orders->links() }}
  @endif
</div>

@endsection