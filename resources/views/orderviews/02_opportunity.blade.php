<?php
use Carbon\carbon;
?>
@extends('layouts.app')
@section('content')

<div class="container">
    <h1>Order {{$order->order_id}}</h1>
    <p>Order placed at <b>{{Carbon::parse($order->ordered_at)->format('d.m.y, H:i')}}</b></p>
</div>

<div class="container" id="order_customer">

    <div class="row">
        <div class="col-md-4">
            <h4>Customer Information</h4>
            <table class="table table-sm table-borderless">
                <tr>
                    <td>Company</td>
                    <td>{{$order->billing_company}}</td>
                </tr>
                <tr>
                    <td>Customer ID</td>
                    <td>{{$order->customer_id}}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{{$order->phone?$order->phone:'-'}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <h4>.</h4>
            <table class="table table-sm table-borderless">
                <tr>
                    <td>Billing E-Mail</td>
                    <td>{{$order->email}}</td>
                </tr>
                <tr>
                    <td>Cardiomatics E-Mail</td>
                    <td>{{$order->cardiomatics_email?$order->cardiomatics_email:'-'}}</td>
                </tr>
                <tr>
                    <td>Contact E-Mail</td>
                    <td>{{$order->contact_email?$order->contact_email:'-'}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <h4>Other Orders ({{count($order->related_orders)-1}})</h4>

            <div class="list-group list-group-flush">
                @foreach($order->related_orders as $related)
                @if($related->order_id==$order->order_id)

                <a href="#" class="list-group-item list-group-item-action active"><b>#{{$related->order_id}}</b>
                    ({{Carbon::parse($related->ordered_at)->format('d.m.Y')}})
                    <span class="badge badge-primary badge-pill"></span></a>
                @else
                <a href="{{route('order.view',$related->id)}}"
                    class="list-group-item list-group-item-action"><b>#{{$related->order_id}}</b>
                    ({{Carbon::parse($related->ordered_at)->format('d.m.Y')}})</a>
                @endif
                @endforeach
                <hr />
            </div>

        </div>
    </div>
    <hr />
    {{-- ADDRESSES --}}
    {{-- ADDRESSES --}}
    <div class="row">
        <div class="col-md-4">
            <h4>Billing</h4>
            <p>
                {{$order->salutation}} {{$order->title}} {{$order->billing_first_name}} {{$order->billing_last_name}}
                <br />
                {{$order->billing_company}} <br />
                {{$order->billing_address_1}} <br />
                {{$order->billing_address2!=""?$order->billing_address_2:'-'}} <br />
                {{$order->billing_postcode}} {{$order->billing_city}}
            </p>
        </div>
        <div class="col-md-4">
            <h4>Shipping</h4>
            <p>
                {{$order->shipping_first_name}} {{$order->shipping_last_name}}
                <br />
                {{$order->shipping_company}} <br />
                {{$order->shipping_address_1}} <br />
                {{$order->shipping_address_2!=""?$order->shipping_address_2:'-'}} <br />
                {{$order->shipping_postcode}} {{$order->shipping_city}}
            </p>
        </div>
        <div class="col-md-4">
            <h4>Products</h4>
            <ul class="list-group">
                @foreach($order->products as $product)
                <li class="list-group-item">
                    {{$product->pivot->quantity}} x
                    {{$product->name}}
                    @if($product->variation)
                    - {{$product->variation}}
                    @endif
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <hr />

    <div class="row">
        <div class="col-md-12">
            <h4>Organization</h4>
        </div>
    </div>
    <div class="row">
            @if($order->organization_id == 0)
                <div class="col-md-6">
                    <p>You have chosen to create a new organization. It will be created with the above data upon synchronization.</p>
                </div>
                <div class="col-md-6">
                    <a role="button" class="btn btn-outline-danger btn-sm" href="{{route('order.disconnect_organization',$order->id)}}" title="Disconnect from Organization">Disconnect</a>
                </div>
            @else
                <div class="col-md-6">
                    <p>You have connected this order to the following organization.</p>
                    <a href="https://v5.b2bsys.net/MainMenu/Organisations/OrganisationCard/OrganisationCard.aspx?orgId={{$order->organization->webcrm_id}}" target="_blank" title="Open in webCRM">{{$order->organization->name}}, {{$order->organization->postcode}} {{$order->organization->city}}</a>
                </div>
                <div class="col-md-6">
                    <a role="button" class="btn btn-outline-primary btn-sm" target="_blank" title="Open in WebCRM (new Tab)" href="https://v5.b2bsys.net/MainMenu/Organisations/OrganisationCard/OrganisationCard.aspx?orgId={{$order->organization->webcrm_id}}"><img src="/images/crm_logo.png" class="img-fluid" alt="webCRM"
                            width="100px;"></a>
                            <a role="button" class="btn btn-outline-danger btn-sm" href="{{route('order.disconnect_organization',$order->id)}}" title="Disconnect from Organization">Disconnect</a>
                </div>
            @endif

    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <h4>Opportunity</h4>
        </div>
    </div>
    <div class="row">
            <div class="col-md-12">
                @if(count($order->organization->opportunities)>0)
                    <p>Select an opportunity or create a new one.</p>
                    <table class="table table-sm">
                        <tr>
                            <td>webCRM</td>
                            <td>Date</td>
                            <td>Level</td>
                            <td>Description</td>
                            <td>Created by</td>
                            <td>Link</td>
                            <td>Select</td>
                        </tr>
                        @foreach($order->organization->opportunities as $opportunity)
                        <tr>
                            <td>{{$opportunity->webcrm_id}}</td>
                            <td>{{Carbon::parse($opportunity->webcrm_created_at)->format('d.m.y')}}</td>
                            <td>{{$opportunity->level}}</td>
                            <td>{{$opportunity->description}}</td>
                            <td>{{$opportunity->created_by}}</td>
                            <td><a role="button" class="btn btn-outline-primary btn-sm" target="_blank" title="Open in WebCRM (new Tab)" href="https://v5.b2bsys.net/MainMenu/Opportunities/OpportunityView.aspx?action=view&eid={{$opportunity->webcrm_id}}"><img src="/images/crm_logo.png" class="img-fluid" alt="webCRM" width="100px;"></a></td>
                            <td><a role="button" class="btn btn-primary btn-sm" title="Choose this organization" href="{{route('order.assign_opportunity', ['order_id'=>$order->id,'opportunity_id' => $opportunity->id])}}" >Continue</a></td>
                        </tr>
                        @endforeach
                    </table>

                @else
                    <p>No opportunities have been found for this organization. Create a new one.</p>
                @endif
                
            </div>
        </div>

    <div class="row">
        <div class="col-md-12">
            <h4>Deliveries</h4>
        </div>
    </div>
    <div class="row">
            <div class="col-md-12">
                @if(count($order->organization->deliveries)>0)
                    <p>For your reference, please find the registered deliveries here.</p>
                    <table class="table table-sm">
                        <tr>
                            <td>webCRM</td>
                            <td>Date</td>
                            <td>Level</td>
                            <td>Description</td>
                            <td>Created by</td>
                            <td>Link</td>
                            {{-- <td>Select</td> --}}
                        </tr>
                        @foreach($order->organization->deliveries as $delivery)
                        <tr>
                            <td>{{$delivery->webcrm_id}}</td>
                            <td>{{Carbon::parse($delivery->webcrm_created_at)->format('d.m.y')}}</td>
                            <td>{{$delivery->level}}</td>
                            <td>{{$delivery->description}}</td>
                            <td>{{$delivery->created_by}}</td>
                            <td><a role="button" class="btn btn-outline-primary btn-sm" target="_blank" title="Open in WebCRM (new Tab)" href="https://v5.b2bsys.net/MainMenu/Deliveries/DeliveryView.aspx?action=view&eid={{$delivery->webcrm_id}}"><img src="/images/crm_logo.png" class="img-fluid" alt="webCRM" width="100px;"></a></td>
                            {{--<td><a role="button" class="btn btn-primary btn-sm" title="Choose this organization" href="{{route('order.assign_opportunity', ['order_id'=>$order->id,'opportunity_id' => $opportunity->id])}}" >Continue</a></td> --}}
                        </tr>
                        @endforeach
                    </table>

                @else
                    <p>No deliveries have been found for this organization.</p>
                @endif
                <a role="button" class="btn btn-primary" title="New Opportunity" href="{{route('order.new_opportunity',$order->id)}}" >New Opportunity</a>
            </div>
        </div>
    </div>


        </div>
    </div>

    <hr />
</div>


@endsection
