<?php

use Carbon\Carbon;
use App\variation;
?>
@extends('layouts.app')
@section('content')

<div class="container">
    <h1>Order {{$order->order_id}}</h1>
    <p>Order placed at <b>{{Carbon::parse($order->ordered_at)->format('d.m.y, H:i')}}</b></p>
</div>

<div class="container" id="order_customer">

    <div class="row">
        <div class="col-md-4">
            <h4>Customer Information</h4>
            <table class="table table-sm table-borderless">
                <tr>
                    <td>Company</td>
                    <td>{{$order->billing_company}}</td>
                </tr>
                <tr>
                    <td>Customer ID</td>
                    <td>{{$order->customer_id}}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{{$order->phone?$order->phone:'-'}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <h4>.</h4>
            <table class="table table-sm table-borderless">
                <tr>
                    <td>Billing E-Mail</td>
                    <td>{{$order->email}}</td>
                </tr>
                <tr>
                    <td>Cardiomatics E-Mail</td>
                    <td>{{$order->cardiomatics_email?$order->cardiomatics_email:'-'}}</td>
                </tr>
                <tr>
                    <td>Contact E-Mail</td>
                    <td>{{$order->contact_email?$order->contact_email:'-'}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <h4>Other Orders ({{count($order->related_orders)-1}})</h4>

            <div class="list-group list-group-flush">
                @foreach($order->related_orders as $related)
                @if($related->order_id==$order->order_id)

                <a href="#" class="list-group-item list-group-item-action active"><b>#{{$related->order_id}}</b>
                    ({{Carbon::parse($related->ordered_at)->format('d.m.Y')}})
                    <span class="badge badge-primary badge-pill"></span></a>
                @else
                <a href="{{route('order.view',$related->id)}}"
                    class="list-group-item list-group-item-action"><b>#{{$related->order_id}}</b>
                    ({{Carbon::parse($related->ordered_at)->format('d.m.Y')}})</a>
                @endif
                @endforeach
                <hr />
            </div>

        </div>
    </div>
    <hr />
    {{-- ADDRESSES --}}
    {{-- ADDRESSES --}}
    <div class="row">
        <div class="col-md-4">
            <h4>Billing</h4>
            <p>
                {{$order->salutation}} {{$order->title}} {{$order->billing_first_name}} {{$order->billing_last_name}}
                <br />
                {{$order->billing_company}} <br />
                {{$order->billing_address_1}} <br />
                {{$order->billing_address2!=""?$order->billing_address_2:'-'}} <br />
                {{$order->billing_postcode}} {{$order->billing_city}}
            </p>
        </div>
        <div class="col-md-4">
            <h4>Shipping</h4>
            <p>
                {{$order->shipping_first_name}} {{$order->shipping_last_name}}
                <br />
                {{$order->shipping_company}} <br />
                {{$order->shipping_address_1}} <br />
                {{$order->shipping_address_2!=""?$order->shipping_address_2:'-'}} <br />
                {{$order->shipping_postcode}} {{$order->shipping_city}}
            </p>
        </div>
        <div class="col-md-4">
            <h4>Products</h4>
            <ul class="list-group">
                @foreach($order->products as $product)
                <li class="list-group-item">
                    {{$product->pivot->quantity}} x
                    {{$product->name}}
                    @if($product->variation)
                    - {{$product->variation}}
                    @endif
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <hr />
    {{-- SELECT ORGANIZATION --}}
    <!-- Hidden Field for jquery to read the current order id -->
    <input type="hidden" name="order_id_hidden" id="order_id_hidden" value="{{$order->id}}">
    <div class="container box">
        <h4>Organization</h4><br />
        <div class="panel panel-default">
            <div class="panel-heading">Search for an organization in webCRM.</div>
            <div class="panel-body">
                <div class="form-group">
                    <input type="text" name="search" id="search" class="form-control"
                        placeholder="Search for name, division name, city, zip, customer number, e-mail adresses, street, ..." />
                </div>
                <div class="table-responsive">
                    <b>Search Results: <span id="total_records"></span></b><br/>Please note that results are limited to 50 entries. If your organization doesnt show up, try to specify your search.
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Division Name</th>
                                <th>Customer Number</th>
                                <th>City</th>
                                <th>Invoice</th>
                                <th>Cardiomatics</th>
                                <th>Links</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <tbody id="organization_search">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <p>
        If you want the order to belong to a new organization, click the following button. 
        </p>
        <a role="button" class="btn btn-primary" title="New Organization" href="{{route('order.new_organization',$order->id)}}" >New Organization</a>
        </div>
    </div>

    <hr />
</div>

{{-- Script for AJAX --}}
<script>
$(document).ready(function(){

 fetch_customer_data();

 function fetch_customer_data(query = '',order_id)
 {
  $.ajax({
   url:"{{ route('live_search.action') }}",
   method:'GET',
   data:{query:query,order_id:order_id},
   dataType:'json',
   success:function(data)
   {
    $('#organization_search').html(data.table_data);
    $('#total_records').text(data.total_data);
   }
  })
 }

 $(document).on('keyup', '#search', function(){
  var query = $(this).val();
  var order_id = $('#order_id_hidden').val();
  fetch_customer_data(query,order_id);
 });
});
</script>


@endsection