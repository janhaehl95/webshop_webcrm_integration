<?php

use Illuminate\Support\Facades\Auth;

?>

<nav class="navbar navbar-expand-lg navbar-light bg-light"  style="background-color: #ffffff; min-height:66px; ">
    <a class="navbar-brand" href="#"><img src="{{asset('images/cortrium_logo_light.png')}}" style="max-height:30px;" class="img-responsive" alt="Responsive image"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            @if(Auth::user())
            <li class="nav-item active">
                <a class="nav-link" href="{{route('home')}}">Orders</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{route('import')}}">Import Webshop Data</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{route('import_webcrm')}}">Import webCRM Data</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{route('credentials')}}">Refresh CRM Connection</a>
            </li>

            @else
            <li class="nav-item active">
                <a class="nav-link" href="/login">Login</a>
            </li>
            @endif
            
        </ul>

    </div>
</nav>