<?php

use Carbon\Carbon;
use App\variation;
?>
@extends('layouts.app')
@section('content')

<div class="container">
    <h1>Order {{$order->order_id}}</h1>
    <p>Organisation ID in WebCRM: {{$order->webcrm_organisation_id}}</p>
    <p>Order placed at <b>{{Carbon::parse($order->ordered_at)->format('d.m.y, H:i')}}</b></p>
</div>

<div class="container" id="order_customer">

    <div class="row">
        <div class="col-md-4">
            <h4>Customer Information</h4>
            <table class="table table-sm table-borderless">
                <tr>
                    <td>Company</td>
                    <td>{{$order->billing_company}}</td>
                </tr>
                <tr>
                    <td>Customer ID</td>
                    <td>{{$order->customer_id}}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{{$order->phone?$order->phone:'-'}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
        <h4>.</h4>
            <table class="table table-sm table-borderless">
                <tr>
                    <td>Billing E-Mail</td>
                    <td>{{$order->email}}</td>
                </tr>
                <tr>
                    <td>Cardiomatics E-Mail</td>
                    <td>{{$order->cardiomatics_email?$order->cardiomatics_email:'-'}}</td>
                </tr>
                <tr>
                    <td>Contact E-Mail</td>
                    <td>{{$order->contact_email?$order->contact_email:'-'}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <h4>Other Orders ({{count($order->related_orders)-1}})</h4>

            <div class="list-group list-group-flush">
                @foreach($order->related_orders as $related)
                @if($related->order_id==$order->order_id)

                <a href="#" class="list-group-item list-group-item-action active"><b>#{{$related->order_id}}</b>
                    ({{Carbon::parse($related->ordered_at)->format('d.m.Y')}})
                    <span class="badge badge-primary badge-pill"></span></a>
                @else
                <a href="{{route('order.view',$related->id)}}"
                    class="list-group-item list-group-item-action"><b>#{{$related->order_id}}</b>
                    ({{Carbon::parse($related->ordered_at)->format('d.m.Y')}})</a>
                @endif
                @endforeach
                <hr />
            </div>

        </div>
    </div>
    <hr />
    {{-- ADDRESSES --}}
    {{-- ADDRESSES --}}
    <div class="row">
        <div class="col-md-4">
            <h4>Billing</h4>
            <p>
                {{$order->salutation}} {{$order->title}} {{$order->billing_first_name}} {{$order->billing_last_name}}
                <br />
                {{$order->billing_company}} <br />
                {{$order->billing_address_1}} <br />
                {{$order->billing_address2!=""?$order->billing_address_2:'-'}} <br />
                {{$order->billing_postcode}} {{$order->billing_city}}
            </p>
        </div>
        <div class="col-md-4">
            <h4>Shipping</h4>
            <p>
                {{$order->shipping_first_name}} {{$order->shipping_last_name}}
                <br />
                {{$order->shipping_company}} <br />
                {{$order->shipping_address_1}} <br />
                {{$order->shipping_address_2!=""?$order->shipping_address_2:'-'}} <br />
                {{$order->shipping_postcode}} {{$order->shipping_city}}
            </p>
        </div>
        <div class="col-md-4">
            <h4>Products</h4>
            <ul class="list-group">
                @foreach($order->products as $product)
                <li class="list-group-item">
                    {{$product->pivot->quantity}} x
                    {{$product->name}}
                    @if($product->variation)
                    - {{$product->variation}}
                    @endif
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <hr />
    {{-- START QUOTATION LINES --}}
    <div class="row">
        <div class="col-md-12">
            <h4>Quotation Lines</h4>
        </div>
    </div>
    {{-- Button Row --}}
    <div class="row mb-1">
        <div class="col-md-6">
            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#linkeddataModal">Add
                Line Item</a>
        </div>
    </div>
    {{-- Table Row --}}
    <div class="row">
        <div class="col-md-12">
            @if(count($order->linkeditems)<1) <p>No Quotation Lines, yet. Click the button "Add Line Items" to get
                started.</p>
                @else
                <p>There are {{count($order->linkeditems)}} line items added.</p>
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">webCRM</th>
                            <th scope="col">Economics</th>
                            <th scope="col">Item Group</th>
                            <th scope="col">Economics Description</th>
                            <th scope="col">Description</th>
                            <th scope="col">Price</th>
                            <th scope="col">Links</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($order->linkeditems as $item)
                        <tr>
                            <td>{{$item->webcrm_id}}</td>
                            <td>{{$item->economic_item_number}}</td>
                            <td>{{$item->item_group}}</td>
                            <td>{{$item->economic_item_description}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->item_price}}</td>
                            <td>
                                <a role="button" class="btn btn-outline-primary btn-sm" target="_blank"
                                    title="View in webCRM"
                                    href="https://v5.b2bsys.net/Configuration/CustomFields/LinkedData/LinkedData_DataRow.aspx?et=7&id={{$item->webcrm_id}}"><img
                                        src="{{asset('images/crm_logo.png')}}" class="img-fluid" alt="webCRM"
                                        width="100px;"></a>
                            </td>
                            <td>
                                <a role="button" class="btn btn-outline-danger btn-sm"
                                    href="{{route('linkeditem.remove',['order_id'=>$order->id,'item_id'=>$item->id])}}">X</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
        </div>
    </div>
    <hr />
    <div class="row">
        <h3>Additional Information</h3>
        <div class="col-md-12">
            <table class="table table-sm table-borderless">
                <tr>
                    <td>Payment</td>
                    <td>{{$order->payment_method_title?$order->payment_method_title:'-'}}</td>
                </tr>
                <tr>
                    <td>MCA Code</td>
                    <td>{{$order->mca_code}}</td>
                </tr>
                <tr>
                    <td>Customer Note</td>
                    <td>{{$order->customer_note?$order->customer_note:'-'}}</td>
                </tr>
                <tr>
                    <td>IP</td>
                    <td>{{$order->customer_ip?$order->customer_ip:'-'}}</td>
                </tr>
                <tr>
                    <td>Agent</td>
                    <td>{{$order->customer_user_agent?$order->customer_user_agent:'-'}}</td>
                </tr>
            </table>
        </div>
    </div>

    <hr />
</div>

{{-- MODALS --}}
<!-- LinkedData Item -->
<div class="modal fade" id="linkeddataModal" tabindex="-1" role="dialog" aria-labelledby="linkeddataModal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" action="{{route('linkeditem.sync',$order->id)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="linkeddataModalLabel">Add Quotation Line Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if(count($linkeddata)<1) <p>No Linked Data Items could be retrieved. Try to import the data from
                        webCRM.</p>
                        @else

                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="searchlinkeditem"
                                    oninput="searchTable('searchlinkeditem','linkeditemslist')"
                                    placeholder="Search...." />
                            </div>
                        </div>
                        <table class="table table-sm" id="linkeditemslist">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">webCRM</th>
                                    <th scope="col">Item Group</th>
                                    <th scope="col">Economics</th>
                                    <th scope="col">Economics Description</th>
                                    <th scope="col">Description</th>
                                    <th scope="action">Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($linkeddata as $item)
                                <tr>
                                    <td><input type="checkbox" name="linkeddataselect[]" value="{{$item->id}}"
                                            aria-label="Select this Item"
                                            {{$order->linkeditems->contains($item->id)?'checked':''}}></td>
                                    <td>{{$item->webcrm_id}}</td>
                                    <td>{{$item->item_group}}</td>
                                    <td>{{$item->economic_item_number}}</td>
                                    <td>{{$item->economic_item_description}}</td>
                                    <td>{{$item->description}}</td>
                                    <td>{{$item->item_price}}</td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>


                        @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection