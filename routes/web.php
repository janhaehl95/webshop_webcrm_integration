<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});
Route::get('/home','HomeController@index')->name('home')->middleware('auth');
Route::post('/','HomeController@browse')->name('home.browse')->middleware('auth');

Route::get('/order/sync/{id}','OrderController@sync')->name('order.sync')->middleware('auth');
Route::get('/import','HomeController@import')->name('import')->middleware('auth');
Route::get('/sync/{order_id}','webcrmcontroller@sync_order')->name('sync')->middleware('auth');
Route::get('/apicredentials','HomeController@create_credentials')->name('credentials')->middleware('auth');
Auth::routes();

Route::get('/import/mcas','McaController@insert')->name('mca.insert')->middleware('auth');

Route::get('/order/{order_id}/mcaconnect/{mca_id}','McaController@connect')->name('mca.connect')->middleware('auth');
Route::get('/order/{order_id}/mcadisconnect','McaController@disconnect')->name('mca.disconnect')->middleware('auth');
//ORDERVIEW
    //Start
    #Route::get('/order/{id}','OrderController@view')->name('order.view')->middleware('auth');
    //Choose
//Assign order to existing organization 
Route::get('/order/{order_id}/sync/organization/{organization_id}','OrderController@assign_organisation')->name('order.assign_organisation')->middleware('auth');
//Assign to create a new organization
Route::get('/order/{order_id}/sync/neworganization','OrderController@new_organisation')->name('order.new_organization')->middleware('auth');
//Disconnect organization
Route::get('/order/{order_id}/sync/disconnectorganization','OrderController@disconnect_organisation')->name('order.disconnect_organization')->middleware('auth');

//Connect to opportunity
Route::get('/order/{order_id}/sync/opportunity/{opportunity_id}','OrderController@assign_opportunity')->name('order.assign_opportunity')->middleware('auth');
Route::get('/order/{order_id}/sync/newopportunity','OrderController@new_opportunity')->name('order.new_opportunity')->middleware('auth');
Route::get('/order/{order_id}/sync/disconnect_opportunity','OrderController@disconnect_opportunity')->name('order.disconnect_opportunity')->middleware('auth');

Route::get('/order/{id}', 'OrderController@view')->name('order.view')->middleware('auth');
Route::get('/live_search/action', 'OrganizationController@search_organisation')->name('live_search.action');

//Product
Route::get('/products','ProductController@index')->name('products.index')->middleware('auth');
Route::get('/products/{product_id}','ProductController@orderview')->name('products.orderview')->middleware('auth');

//WebCRM Organizations
Route::get('/organizations','OrganizationController@index')->name('organizations.index')->middleware('auth');
Route::get('/organizations/import','OrganizationController@import')->name('organizations.import')->middleware('auth');
#Route::get('/sync/{order_id}','webcrmcontroller@create_quotation')->name('sync')->middleware('auth');

//Import LinkedData
Route::get('/import/webcrm','HomeController@import_webcrm')->name('import_webcrm')->middleware('auth');

//Update LinkedData (Connect w Order)
Route::post('order/quotationline/{order_id}','LinkeditemController@sync')->name('linkeditem.sync')->middleware('auth');
Route::get('order/quotationline/remove/{order_id}/{item_id}','LinkeditemController@remove')->name('linkeditem.remove')->middleware('auth');

Route::get('/dmd','dmdcontroller@index');